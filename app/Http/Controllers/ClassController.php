<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ClassModel;
use App\StudentModel;

class ClassController extends Controller
{
    private $classClass;
    private $studentClass;
    public function __construct(){
        $this->classClass = new ClassModel();
        $this->studentClass = new StudentModel();
    }
    
    public function index(Request $request){
        if($request->ajax()){
            return $this->classClass->get_classes_list($request);
        }
        
        $result = array ();
        $result["classes"] = $this->classClass->get_classes_list($request);
        
        return view("class", $result);
    }
    
    public function save(Request $request){
        $this->classClass->save_class($request);
        $result = array(
            "ERROR" => false,
            "MSG" => "ទិន្នន័យបានបញ្ចូលរួចរាល់។"
        );
        
        return $result;
    }
    
    public function get_cls_by_id(Request $request){
        return $this->classClass->get_class_by_id($request);
    }
    
    public function update_cls(Request $request){
        $result = array();
        if($this->classClass->update_class($request) != 0)
        {
            $result = array(
                "ERROR" => false,
                "MSG" => "ទិន្នន័យបានកែសម្រួលរួចរាល់។"
            );
        }
        return $result;
    }
    
    public function request_students_list(Request $request, $id){
        if($request->ajax()){
            $res = $this->studentClass->get_students_list($request, $id);
            return $res;
        }
        
        $result = array();
        
        $result["students"] = $this->studentClass->get_students_list($request, $id);
        $result["ID"] = $id;
        
//         $result['students'] = collect($result['students'])->sortBy('std_nm');

        
        return view("index", $result);
    }
}
