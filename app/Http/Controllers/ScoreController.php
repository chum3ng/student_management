<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ClassModel;
use App\StudentModel;
use App\ScoreModel;

class ScoreController extends Controller
{
    private $scoreClass;
    private $studentClass;
    public function __construct(){
        $this->scoreClass = new ScoreModel();
        $this->studentClass = new StudentModel();
    }
    
    public function index(Request $request){

        if($request->ajax()){
//             echo $request->MONTH_SCORE;
//             exit();
            return $this->scoreClass->get_students($request, "std_nm", "ASC");
        }
        
        $result = array ();
        $result["stds"] = $this->scoreClass->get_students($request, "std_nm", "ASC");
        
        return view("score1", $result);
    }
    
    public function save(Request $request){
        $this->scoreClass->save_score($request);
        $result = array(
            "ERROR" => false,
            "MSG" => "ទិន្នន័យបានបញ្ចូលរួចរាល់។"
        );
        
        return $result;
    }
    
    public function update(Request $request){
        $result = array();
        if($this->scoreClass->update_score($request) != 0)
        {
            $result = array(
                "ERROR" => false,
                "MSG" => "ទិន្នន័យបានកែសម្រួលរួចរាល់។"
            );
        }
        return $result;
    }
}
