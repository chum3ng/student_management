<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\StudentModel;

class StudentController extends Controller
{
    private $studentClass;
    
    public function __construct(){
        $this->studentClass = new StudentModel();
    }
    
//     public function index(Request $request, $id){
//         if($request->ajax()){
//             return $this->studentClass->get_students_list($request);
//         }
        
//         $result = array ();
//         $result["students"] = $this->studentClass->get_students_list($request);
        
        
//         return view("index", $result);
//     }
    
    public function get_std_by_id(Request $request){
        return $this->studentClass->get_student_by_id($request);
    }
    
    public function save(Request $request){
        $this->studentClass->save_student($request);
        $result = array(
            "ERROR" => false,
            "MSG" => "ទិន្នន័យបានបញ្ចូលរួចរាល់។"
        );
        
        return $result;
    }
    
    public function update(Request $request){
        $result = array();
        if($this->studentClass->update_student($request) != 0)
        {
            $result = array(
                "ERROR" => false,
                "MSG" => "ទិន្នន័យបានកែសម្រួលរួចរាល់។"
                );
        }
        return $result;
    }
   
}
