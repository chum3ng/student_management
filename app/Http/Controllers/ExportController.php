<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\StudentExport;
use App\Exports\ScoreExport;
use App\Exports\GradeExport;

class ExportController extends Controller
{
    public function export(Request $request)
    {
        $obj = json_decode($request->json);
        
        return Excel::download(new StudentExport($obj), 'students_list_'. date("Ymdmis"). '.xlsx');
    }
    
    public function exportScore(Request $request)
    {
        $obj = json_decode($request->json);
        
        return Excel::download(new ScoreExport($obj), 'score_list_'. date("Ymdmis") . '.xlsx');
    }
    
    public function exportGrade(Request $request)
    {
        $obj = json_decode($request->json);
        
        return Excel::download(new GradeExport($obj), 'grade_list_'. date("Ymdmis") . '.xlsx');
    }
}