<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //TODO: log sql separately
        DB::listen(function($query){
            
            $logString = '[' .date("Y-m-d H:m:s", time()) .'] '.PHP_EOL;
            $logString .= $query->sql.PHP_EOL.PHP_EOL;
            /* $logString.= json_encode($query->bindings).PHP_EOL.PHP_EOL; */
            $logString .= "----------------------------------------------".PHP_EOL;
            
            $param_binding = "";
            $params_size = sizeof($query->bindings);
            if($params_size > 0)
            {
                $param_binding .= "/*  Input Parameters  */".PHP_EOL.PHP_EOL;
                $param_binding .= "{".PHP_EOL;
                
                $i=0;
                foreach ($query->bindings as $key => $value)
                {
                    if($i == $params_size-1){
                        $param_binding .= '    "'.$key.'" : "'.$value.'"'.PHP_EOL;
                    }else{
                        $param_binding .= '    "'.$key.'" : "'.$value.'",'.PHP_EOL;
                    }
                    
                    $i++;
                }
                $param_binding .= "}".PHP_EOL;
                $param_binding .= "----------------------------------------------".PHP_EOL.PHP_EOL;
            }
            
            File::append(
                storage_path('/logs/query_' .date("Ymd", time()) .'.log'),
                $logString.$param_binding
                );
        });
    }
}
