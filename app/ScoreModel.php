<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class ScoreModel extends Model
{
    public function __contruct(){
        
    }
    
    public function get_passed_avg($request = null, $gender, $pass_symbol, $fail_symbol){
        $result = DB::table('sms_students')->select(DB::raw('count(std_nm) as avg'))
                                            ->join('sms_scores', 'sms_students.id', '=', 'sms_scores.std_id')
                                            ->join('sms_classes', 'sms_students.class_id', '=', 'sms_classes.id')
                                            ->where('month_score', '=', $request->MONTH_SCORE)
                                            ->where('year', '=', $request->YEAR)
                                            ->where('class_nm', '=', $request->CLS_NM);
        
        if(isset($gender)){
            $result = $result->where('std_gender', '=', $gender);
        }
        
        if(isset($pass_symbol)){
            $result = $result->where('std_average', $pass_symbol, 25);
        }
        
        if(isset($fail_symbol)){
            $result = $result->where('std_average', $fail_symbol, 25);
        }
        
        return $result->get();
    }
    
    public function get_students($request = null, $orderBy = null, $order = null){
        $result = DB::table('sms_students')->select('sms_students.id', 'std_nm', 'std_gender', 'sms_scores.id as scr_id', 'class_nm', 'year', 'month_score', 'year_score'
                                                    , 'std_math', 'std_physic', 'std_chemistry', 'std_biology', 'std_khmer', 'std_home'
                                                    , 'std_citizen', 'std_history', 'std_geography', 'std_science', 'std_english', 'std_art'
                                                    , 'std_exercise', 'std_lifelesson', 'std_agriculture', 'std_total', 'dividend', 'std_average')
                                            ->join('sms_classes', 'sms_students.class_id', '=', 'sms_classes.id')
                                            ->leftJoin('sms_scores', function($join) use($request){
                                                $join->on('sms_students.id', '=', 'sms_scores.std_id');
                                                if(isset($request->MONTH_SCORE)){
                                                    $join->on('sms_scores.month_score', '=', DB::raw($request->MONTH_SCORE));
                                                } else {
                                                    $join->on('sms_scores.month_score', '=', DB::raw(Carbon::now()->month));
                                                }
                                            });
        
        if(isset($request->ID)){
            $result = $result->where('sms_scores.id', '=', $request->ID);
        }
                                            
        if(isset($request->STD_NM) == 1){
            $result = $result->where('std_nm', '=', $request->STD_NM);
        }
        
        if(isset($request->CLS_NM) == 1){
            $result = $result->where('class_nm', '=', $request->CLS_NM);
        }
        
        if(isset($request->YEAR) == 1){
            $result = $result->where('year', '=', $request->YEAR);
        }
        
        if(isset($orderBy) == 1 && isset($order) == 1){
            $result = $result->orderByRaw($orderBy.' '.$order);
        }
        
        return $result->get();
    }
    
    public function save_score($request = null){
        $id = DB::table('sms_scores')->insertGetId(
            [
                'month_score'       => $request->MONTH_SCORE,
                'year_score'        => now()->year,
                'std_math'          => $request->MATH,
                'std_physic'        => $request->PHYSIC,
                'std_chemistry'     => $request->CHEMISTRY,
                'std_biology'       => $request->BIOLOGY,
                'std_khmer'         => $request->KHMER,
                'std_home'          => $request->HOME,
                'std_citizen'       => $request->CITIZEN,
                'std_history'       => $request->HISTORY,
                'std_geography'     => $request->GEOGRAPHY,
                'std_science'       => $request->SCIENCE,
                'std_english'       => $request->ENGLISH,
                'std_exercise'      => $request->EXERCISE,
                'std_art'           => $request->ART,
                'std_lifelesson'    => $request->LIFELESSON,
                'std_agriculture'   => $request->AGRICULTURE,
                'std_total'         => $request->TOTAL,
                'dividend'          => $request->DIVIDEND,
                'std_average'       => $request->AVERAGE,
                'std_id'            => $request->STD_ID
                
            ]);
        
        return $id;
    }
    
    public function update_score($request = null){
        $result = DB::table('sms_scores')
        ->where('id', $request->ID)
        ->update(
            [
                'month_score'       => $request->MONTH_SCORE,
                'year_score'        => now()->year,
                'std_math'          => $request->MATH,
                'std_physic'        => $request->PHYSIC,
                'std_chemistry'     => $request->CHEMISTRY,
                'std_biology'       => $request->BIOLOGY,
                'std_khmer'         => $request->KHMER,
                'std_home'          => $request->HOME,
                'std_citizen'       => $request->CITIZEN,
                'std_history'       => $request->HISTORY,
                'std_geography'     => $request->GEOGRAPHY,
                'std_science'       => $request->SCIENCE,
                'std_english'       => $request->ENGLISH,
                'std_exercise'      => $request->EXERCISE,
                'std_art'           => $request->ART,
                'std_lifelesson'    => $request->LIFELESSON,
                'std_agriculture'   => $request->AGRICULTURE,
                'std_total'         => $request->TOTAL,
                'std_average'       => $request->AVERAGE
            ]);
        
        return $result;
    }
    
    public function get_students_with_rank($request = null, $orderBy = null, $order = null){
        $result = DB::table('sms_students')->select('sms_students.id', 'std_nm', 'std_gender', 'sms_scores.id as scr_id', 'class_nm', 'year', 'month_score', 'year_score'
                            , 'std_math', 'std_physic', 'std_chemistry', 'std_biology', 'std_khmer', 'std_home'
                            , 'std_citizen', 'std_history', 'std_geography', 'std_science', 'std_english', 'std_art'
                            , 'std_exercise', 'std_lifelesson', 'std_agriculture', 'std_total', 'dividend', 'std_average'
                            , DB::raw('FIND_IN_SET(std_average, (select GROUP_CONCAT(std_average ORDER BY std_average DESC) FROM sms_scores where month_score='. DB::raw($request->MONTH_SCORE) .')) AS std_rank'))
                        ->join('sms_classes', 'sms_students.class_id', '=', 'sms_classes.id')
                        ->leftJoin('sms_scores', function($join) use($request){
                               $join->on('sms_students.id', '=', 'sms_scores.std_id');
                                if(isset($request->MONTH_SCORE)){
                                    $join->on('sms_scores.month_score', '=', DB::raw($request->MONTH_SCORE));
                                } else {
                                    $join->on('sms_scores.month_score', '=', DB::raw(Carbon::now()->month));
                                }
                        });
                
            if(isset($request->ID)){
                $result = $result->where('sms_scores.id', '=', $request->ID);
            }
            
            if(isset($request->STD_NM) == 1){
                $result = $result->where('std_nm', '=', $request->STD_NM);
            }
            
            if(isset($request->CLS_NM) == 1){
                $result = $result->where('class_nm', '=', $request->CLS_NM);
            }
            
            if(isset($request->YEAR) == 1){
                $result = $result->where('year', '=', $request->YEAR);
            }
            
            if(isset($orderBy) == 1 && isset($order) == 1){
                $result = $result->orderByRaw($orderBy.' '.$order);
            }
            
            return $result->get();
    }
}
