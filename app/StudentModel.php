<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class StudentModel extends Model
{
    public function __contruct(){
        
    }
    
    public function save_student($request = null){
        $id = DB::table('sms_students')->insertGetId(
                [
                    'std_nm' => $request->STD_NAME, 
                    'std_dob' => $request->STD_DOB, 
                    'std_gender' => $request->STD_GENDER, 
                    'std_father_name' => $request->FATHER_NAME,
                    'std_father_job' => $request->FATHER_JOB,
                    'std_father_phone' => $request->FATHER_PHONE,
                    'std_mother_name' => $request->MOTHER_NAME,
                    'std_mother_job' => $request->MOTHER_JOB,
                    'std_mother_phone' => $request->MOTHER_PHONE,
                    'std_birth_village' => $request->BIRTH_VILLAGE,
                    'std_birth_commune' => $request->BIRTH_COMMUNE,
                    'std_birth_district' => $request->BIRTH_DISTRICT,
                    'std_birth_province' => $request->BIRTH_PROVINCE,
                    'std_now_village' => $request->NOW_VILLAGE,
                    'std_now_commune' => $request->NOW_COMMUNE,
                    'std_now_district' => $request->NOW_DISTRICT,
                    'std_now_province' => $request->NOW_PROVINCE,
                    'class_id' => $request->CLS_ID
                ]
            );
        
        return $id;
    }
    
    public function get_students_list($request = null, $id){
        return DB::table('sms_students')->select()->where('class_id', '=', $id)->get();
    }
    
    public function get_student_by_id($request = null){
        return DB::table('sms_students')->select()->where('id', '=', $request->ID)->get();
    }
    
    public function update_student($request = null){
        $result = DB::table('sms_students')
            ->where('id', $request->ID)
            ->update(
                [
                    'std_nm' => $request->STD_NAME,
                    'std_dob' => $request->STD_DOB,
                    'std_gender' => $request->STD_GENDER,
                    'std_father_name' => $request->FATHER_NAME,
                    'std_father_job' => $request->FATHER_JOB,
                    'std_father_phone' => $request->FATHER_PHONE,
                    'std_mother_name' => $request->MOTHER_NAME,
                    'std_mother_job' => $request->MOTHER_JOB,
                    'std_mother_phone' => $request->MOTHER_PHONE,
                    'std_birth_village' => $request->BIRTH_VILLAGE,
                    'std_birth_commune' => $request->BIRTH_COMMUNE,
                    'std_birth_district' => $request->BIRTH_DISTRICT,
                    'std_birth_province' => $request->BIRTH_PROVINCE,
                    'std_now_village' => $request->NOW_VILLAGE,
                    'std_now_commune' => $request->NOW_COMMUNE,
                    'std_now_district' => $request->NOW_DISTRICT,
                    'std_now_province' => $request->NOW_PROVINCE,
                    'class_id' => $request->CLS_ID
                ]
            );
        
        return $result;
    }
    
}
