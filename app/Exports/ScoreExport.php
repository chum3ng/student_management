<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Events\AfterSheet;
use PHPExcel_Style_Border;
use PHPExcel_Style_NumberFormat;
use App\StudentModel;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use App\ScoreModel;

class ScoreExport implements FromCollection, WithHeadings, WithEvents
{
    use Exportable;
    private $param = null;
    private $row_cnt = 0;
    private $month = "";
    
    public function __construct($param)
    {
        $this->param = $param;
    }
    
    public function headings(): array
    {
        return [
            'ល.រ',
            'ឈ្មោះ',
            'ភេទ',
            'គណិត',
            'រូប',
            'គីមី',
            'ជីវ',
            'ខ្មែរ',
            'គេហ',
            'ប្រវត្តិ',
            'ភូមិ',
            'ពលរដ្ឋ',
            'ផែនដី',
            'អ.ក',
            'អ.គ',
            'សិល្បៈ',
            'បំណិន',
            'កសិ',
            'សរុប',
            'ម.ភាគ',
            'ចំ.ថ្នាក់'
        ];
    }
    
    public function registerEvents(): array
    {
        $styleArr= [
            'borders' => [
                'outline' => [
                    'borderStyle' => PHPExcel_Style_Border::BORDER_THIN,
                ] ,
            ],
            
            'font' => [
                'name' => 'Khmer OS Battambang',
                'size' => 10
            ],
            
            'alignment' => [
                'vertical' => Alignment::VERTICAL_CENTER,
            ],
        ];
        
        $bold = [
            'font' => [
                'bold' => true
            ],
            
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
                'vertical' => Alignment::VERTICAL_CENTER,
            ],
        ];
        
        $center = [
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
                'vertical' => Alignment::VERTICAL_CENTER,
            ],
        ];
        
        return [
            AfterSheet::class=>function(AfterSheet $event) use ($styleArr,$bold, $center)
            {
                
                if($this->param->MONTH_SCORE == '1') $this->month = "មករា";
                else if($this->param->MONTH_SCORE == '2') $this->month = "កុម្ភះ";
                else if($this->param->MONTH_SCORE == '3') $this->month = "មិនា";
                else if($this->param->MONTH_SCORE == '4') $this->month = "មេសា";
                else if($this->param->MONTH_SCORE == '5') $this->month = "ឧសភា";
                else if($this->param->MONTH_SCORE == '6') $this->month = "មិថុនា";
                else if($this->param->MONTH_SCORE == '7') $this->month = "កក្កដា";
                else if($this->param->MONTH_SCORE == '8') $this->month = "សីហា";
                else if($this->param->MONTH_SCORE == '9') $this->month = "កញ្ញា";
                else if($this->param->MONTH_SCORE == '10') $this->month = "តុលា";
                else if($this->param->MONTH_SCORE == '11') $this->month = "វិច្ចិកា";
                else if($this->param->MONTH_SCORE == '12') $this->month = "ធ្នូ";
                else if($this->param->MONTH_SCORE == '13') $this->month = "ឆមាសទី១";
                else if($this->param->MONTH_SCORE == '14') $this->month = "ឆមាសទី២";
                
                $event->sheet->getStyle('A1:V1')->applyFromArray($bold);
                $event->sheet->getDelegate()->getPageSetup()->setHorizontalCentered(true);
                $event->sheet->getDelegate()->getPageSetup()->setPaperSize(\PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
                $event->sheet->getDelegate()->getPageMargins()->setTop(1.3);
                $event->sheet->getDelegate()->getPageMargins()->setBottom(0.75);
                $event->sheet->getDelegate()->getPageMargins()->setLeft(0.25);
                $event->sheet->getDelegate()->getPageMargins()->setRight(0.25);
                $event->sheet->getDelegate()->getPageMargins()->setHeader(0.3);
                $event->sheet->getDelegate()->getPageMargins()->setFooter(0.3);
                
                $event->sheet->getDelegate()->getPageSetup()->setFitToWidth(1);
                //                 $event->sheet->getDelegate()->getHeaderFooter()->setOddHeader("This is font testing.");
                $event->sheet->getDelegate()->getHeaderFooter()->setOddHeader("&L&\"Khmer M1, -\"\nមន្ទីរអប់រំយុវជន និងកីឡារាជធានីភ្នំពេញ \nវិទ្យាល័យ កំបូល &C&\"Khmer M1, -\"\n\n\nបញ្ជីពិន្ទុសិស្សប្រចាំខែ" . $this->month . " ឆ្នាំសិក្សា " . $this->param->YEAR . " \nថ្នាក់ទី " . $this->param->CLS_NM . " &R&\"Khmer M1, -\"ព្រះរាជាណាចក្រកម្ពុជា \nជាតិ សាសនា ព្រះមហាក្សត្រ");
                
                for($i = 1; $i <= $this->row_cnt +1; $i++)
                {
                    //Convert Number to Text
                    $event->sheet->getStyle('A'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                    $event->sheet->getStyle('B'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                    $event->sheet->getStyle('C'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                    $event->sheet->getStyle('D'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                    $event->sheet->getStyle('E'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                    $event->sheet->getStyle('F'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                    $event->sheet->getStyle('G'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                    $event->sheet->getStyle('H'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                    $event->sheet->getStyle('I'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                    $event->sheet->getStyle('J'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                    $event->sheet->getStyle('K'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                    $event->sheet->getStyle('L'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                    $event->sheet->getStyle('M'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                    $event->sheet->getStyle('N'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                    $event->sheet->getStyle('O'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                    $event->sheet->getStyle('P'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                    $event->sheet->getStyle('Q'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                    $event->sheet->getStyle('R'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                    $event->sheet->getStyle('S'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                    $event->sheet->getStyle('T'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                    $event->sheet->getStyle('U'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                    
                    //Add Border for All Cells
                    $event->sheet->getStyle('A'.$i)->applyFromArray($styleArr);
                    $event->sheet->getStyle('B'.$i)->applyFromArray($styleArr);
                    $event->sheet->getStyle('C'.$i)->applyFromArray($styleArr);
                    $event->sheet->getStyle('D'.$i)->applyFromArray($styleArr);
                    $event->sheet->getStyle('E'.$i)->applyFromArray($styleArr);
                    $event->sheet->getStyle('F'.$i)->applyFromArray($styleArr);
                    $event->sheet->getStyle('G'.$i)->applyFromArray($styleArr);
                    $event->sheet->getStyle('H'.$i)->applyFromArray($styleArr);
                    $event->sheet->getStyle('I'.$i)->applyFromArray($styleArr);
                    $event->sheet->getStyle('J'.$i)->applyFromArray($styleArr);
                    $event->sheet->getStyle('K'.$i)->applyFromArray($styleArr);
                    $event->sheet->getStyle('L'.$i)->applyFromArray($styleArr);
                    $event->sheet->getStyle('M'.$i)->applyFromArray($styleArr);
                    $event->sheet->getStyle('N'.$i)->applyFromArray($styleArr);
                    $event->sheet->getStyle('O'.$i)->applyFromArray($styleArr);
                    $event->sheet->getStyle('P'.$i)->applyFromArray($styleArr);
                    $event->sheet->getStyle('Q'.$i)->applyFromArray($styleArr);
                    $event->sheet->getStyle('R'.$i)->applyFromArray($styleArr);
                    $event->sheet->getStyle('S'.$i)->applyFromArray($styleArr);
                    $event->sheet->getStyle('T'.$i)->applyFromArray($styleArr);
                    $event->sheet->getStyle('U'.$i)->applyFromArray($styleArr);
                    
                    //Arrange cells
                    $event->sheet->getStyle('A'.$i)->applyFromArray($center);
                    $event->sheet->getStyle('C'.$i)->applyFromArray($center);
                    $event->sheet->getStyle('D'.$i)->applyFromArray($center);
                    $event->sheet->getStyle('E'.$i)->applyFromArray($center);
                    $event->sheet->getStyle('F'.$i)->applyFromArray($center);
                    $event->sheet->getStyle('G'.$i)->applyFromArray($center);
                    $event->sheet->getStyle('H'.$i)->applyFromArray($center);
                    $event->sheet->getStyle('I'.$i)->applyFromArray($center);
                    $event->sheet->getStyle('J'.$i)->applyFromArray($center);
                    $event->sheet->getStyle('K'.$i)->applyFromArray($center);
                    $event->sheet->getStyle('L'.$i)->applyFromArray($center);
                    $event->sheet->getStyle('M'.$i)->applyFromArray($center);
                    $event->sheet->getStyle('N'.$i)->applyFromArray($center);
                    $event->sheet->getStyle('O'.$i)->applyFromArray($center);
                    $event->sheet->getStyle('P'.$i)->applyFromArray($center);
                    $event->sheet->getStyle('Q'.$i)->applyFromArray($center);
                    $event->sheet->getStyle('R'.$i)->applyFromArray($center);
                    $event->sheet->getStyle('S'.$i)->applyFromArray($center);
                    $event->sheet->getStyle('T'.$i)->applyFromArray($center);
                    $event->sheet->getStyle('U'.$i)->applyFromArray($center);
                    
                    //Set Size
                    $event->sheet->getColumnDimension('A')->setWidth(5);
                    $event->sheet->getColumnDimension('B')->setWidth(25);
                    $event->sheet->getColumnDimension('C')->setWidth(5);
                    $event->sheet->getColumnDimension('D')->setWidth(8);
                    $event->sheet->getColumnDimension('E')->setWidth(8);
                    $event->sheet->getColumnDimension('F')->setWidth(8);
                    $event->sheet->getColumnDimension('G')->setWidth(8);
                    $event->sheet->getColumnDimension('H')->setWidth(8);
                    $event->sheet->getColumnDimension('I')->setWidth(8);
                    $event->sheet->getColumnDimension('J')->setWidth(8);
                    $event->sheet->getColumnDimension('K')->setWidth(8);
                    $event->sheet->getColumnDimension('L')->setWidth(8);
                    $event->sheet->getColumnDimension('M')->setWidth(8);
                    $event->sheet->getColumnDimension('N')->setWidth(8);
                    $event->sheet->getColumnDimension('O')->setWidth(8);
                    $event->sheet->getColumnDimension('P')->setWidth(8);
                    $event->sheet->getColumnDimension('Q')->setWidth(8);
                    $event->sheet->getColumnDimension('R')->setWidth(8);
                    $event->sheet->getColumnDimension('S')->setWidth(9);
                    $event->sheet->getColumnDimension('T')->setWidth(9);
                    $event->sheet->getColumnDimension('U')->setWidth(7);
                }
        }
        ];
    }
    
    public function collection()
    {
        $columns = array();
        //array_push($columns, "no", "std_nm", "std_gender", "std_dob", "std_father_name", "std_father_job", "std_father_phone", "std_mother_name", "std_mother_job", "std_mother_phone", "std_birth_village", "std_birth_commune", "std_birth_district", "std_birth_province", "std_now_village", "std_now_commune", "std_now_district", "std_now_province");
        array_push($columns, "no", "std_nm", "std_gender", "std_math", "std_khmer", "std_physic",
            "std_chemistry", "std_biology", "std_science", "std_geography", "std_history",
            "std_citizen", "std_home", "std_english", "std_exercise", "std_art", "std_lifelesson",
            "std_agriculture", "std_total", "std_average", "std_grade");
        
        $scores = new ScoreModel();
        $result = collect($scores->get_students($this->param))->sortByDesc('std_average');
//         echo $result;
//         exit();
        
        $set_columns = array();
        $i = 0;
        foreach($result as $student)
        {
            $set_item = array();
            foreach($columns as $column)
            {
                if($column == "no")
                {
                    $set_item[$column] = $this->row_cnt + 1;
                }
                else
                {
                    if($column == "std_gender")
                    {
                        if($student->$column == "1"){
                            $set_item[$column] = "ប្រុស";
                        }
                        else {
                            $set_item[$column] = "ស្រី";
                        }
                    }
                    else if($column == "std_grade")
                    {
                        $i = $i + 1;
                        $set_item[$column] = $i;
                    }
                    else
                    {
                        $set_item[$column] = $student->$column;
                    }
                }
            }
            
            array_push($set_columns, (object) $set_item);
            $this->row_cnt++;
        }
        
        return collect($set_columns);
    }
    
    private function formatDateTime($date)
    {
        return date('d-m-Y h:i:s A', strtotime($date));
    }
    
    private function formatDate($date)
    {
        return date('d-m-Y', strtotime($date));
    }
    
    private function formatTime($time)
    {
        return date('H:i', strtotime($time));
    }
}