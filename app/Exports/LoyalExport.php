<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Events\AfterSheet;
use PHPExcel_Style_Border;
use PHPExcel_Style_NumberFormat;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use App\ScoreModel;

class LoyalExport implements WithHeadings, WithEvents//FromCollection, 
{
    use Exportable;
    private $param = null;
    private $row_cnt = 0;
    private $month = "";
    
    public function __construct($param)
    {
        $this->param = $param;
    }
    
    public function headings(): array
    {
        return [
            'ល.រ',
            'ឈ្មោះ',
            'ភេទ',
            'ពិន្ទុសរុប',
            'មធ្យមភាគ',
            'ចំណាត់ថ្នាក់',
            'ល.រ',
            'ឈ្មោះ',
            'ភេទ',
            'ពិន្ទុសរុប',
            'មធ្យមភាគ',
            'ចំណាត់ថ្នាក់'
        ];
    }
    
    public function registerEvents(): array
    {
        $styleArr= [
            'borders' => [
                'outline' => [
                    'borderStyle' => PHPExcel_Style_Border::BORDER_THIN,
                ] ,
            ],
            
            'font' => [
                'name' => 'Khmer OS Battambang',
                'size' => 10
            ],
            
            'alignment' => [
                'vertical' => Alignment::VERTICAL_CENTER,
            ],
        ];
        
        $bold = [
            'font' => [
                'bold' => true
            ],
            
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
                'vertical' => Alignment::VERTICAL_CENTER,
            ],
        ];
        
        $center = [
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
                'vertical' => Alignment::VERTICAL_CENTER,
            ],
        ];
        
        return [
            AfterSheet::class=>function(AfterSheet $event) use ($styleArr,$bold, $center)
            {
                
                if($this->param->MONTH_SCORE == '1') $this->month = "មករា";
                else if($this->param->MONTH_SCORE == '2') $this->month = "កុម្ភះ";
                else if($this->param->MONTH_SCORE == '3') $this->month = "មិនា";
                else if($this->param->MONTH_SCORE == '4') $this->month = "មេសា";
                else if($this->param->MONTH_SCORE == '5') $this->month = "ឧសភា";
                else if($this->param->MONTH_SCORE == '6') $this->month = "មិថុនា";
                else if($this->param->MONTH_SCORE == '7') $this->month = "កក្កដា";
                else if($this->param->MONTH_SCORE == '8') $this->month = "សីហា";
                else if($this->param->MONTH_SCORE == '9') $this->month = "កញ្ញា";
                else if($this->param->MONTH_SCORE == '10') $this->month = "តុលា";
                else if($this->param->MONTH_SCORE == '11') $this->month = "វិច្ចិកា";
                else if($this->param->MONTH_SCORE == '12') $this->month = "ធ្នូ";
                else if($this->param->MONTH_SCORE == '13') $this->month = "ឆមាសទី១";
                else if($this->param->MONTH_SCORE == '14') $this->month = "ឆមាសទី២";
                
                //For Header
                $event->sheet->getStyle('A1:L1')->applyFromArray($bold);
                //Convert Number to Text
                $event->sheet->getStyle('A1')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                $event->sheet->getStyle('B1')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                $event->sheet->getStyle('C1')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                $event->sheet->getStyle('D1')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                $event->sheet->getStyle('E1')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                $event->sheet->getStyle('F1')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                $event->sheet->getStyle('G1')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                $event->sheet->getStyle('H1')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                $event->sheet->getStyle('I1')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                $event->sheet->getStyle('J1')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                $event->sheet->getStyle('K1')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                $event->sheet->getStyle('L1')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                
                //Add Border for All Cells
                $event->sheet->getStyle('A1')->applyFromArray($styleArr);
                $event->sheet->getStyle('B1')->applyFromArray($styleArr);
                $event->sheet->getStyle('C1')->applyFromArray($styleArr);
                $event->sheet->getStyle('D1')->applyFromArray($styleArr);
                $event->sheet->getStyle('E1')->applyFromArray($styleArr);
                $event->sheet->getStyle('F1')->applyFromArray($styleArr);
                $event->sheet->getStyle('G1')->applyFromArray($styleArr);
                $event->sheet->getStyle('H1')->applyFromArray($styleArr);
                $event->sheet->getStyle('I1')->applyFromArray($styleArr);
                $event->sheet->getStyle('J1')->applyFromArray($styleArr);
                $event->sheet->getStyle('K1')->applyFromArray($styleArr);
                $event->sheet->getStyle('L1')->applyFromArray($styleArr);
                
                //Arrange cells
                $event->sheet->getStyle('A1')->applyFromArray($center);
                $event->sheet->getStyle('C1')->applyFromArray($center);
                $event->sheet->getStyle('D1')->applyFromArray($center);
                $event->sheet->getStyle('E1')->applyFromArray($center);
                $event->sheet->getStyle('F1')->applyFromArray($center);
                $event->sheet->getStyle('G1')->applyFromArray($center);
                $event->sheet->getStyle('I1')->applyFromArray($center);
                $event->sheet->getStyle('J1')->applyFromArray($center);
                $event->sheet->getStyle('K1')->applyFromArray($center);
                $event->sheet->getStyle('L1')->applyFromArray($center);
                //End Header
                
                
                $event->sheet->getDelegate()->getPageSetup()->setHorizontalCentered(true);
                $event->sheet->getDelegate()->getPageMargins()->setTop(1.7);
                $event->sheet->getDelegate()->getPageMargins()->setBottom(0.75);
                $event->sheet->getDelegate()->getPageMargins()->setLeft(0.25);
                $event->sheet->getDelegate()->getPageMargins()->setRight(0.25);
                $event->sheet->getDelegate()->getPageMargins()->setHeader(0.3);
                $event->sheet->getDelegate()->getPageMargins()->setFooter(0.3);
                
                $event->sheet->getDelegate()->getPageSetup()->setFitToWidth(1);
                //                 $event->sheet->getDelegate()->getHeaderFooter()->setOddHeader("This is font testing.");
                $event->sheet->getDelegate()->getHeaderFooter()->setOddHeader("&L&\"Khmer M1, -\"\nមន្ទីរអប់រំយុវជន និងកីឡារាជធានីភ្នំពេញ \nវិទ្យាល័យ កំបូល &C&\"Khmer M1, -\"\n\n\nបញ្ជីពិន្ទុសិស្សប្រចាំខែ" . $this->month . " ឆ្នាំសិក្សា " . $this->param->YEAR . " \nថ្នាក់ទី " . $this->param->CLS_NM . " &R&\"Khmer M1, -\"ព្រះរាជាណាចក្រកម្ពុជា \nជាតិ សាសនា ព្រះមហាក្សត្រ");
                
                $scores = new ScoreModel();
                $result = collect($scores->get_students($this->param))->sortByDesc('std_average');
                
                $i = 0;
                $j = 1;
                foreach($result as $student)
                {
                    $i++;
                    
                    if($i <= 5)
                    {
                        $event->sheet->getDelegate()->setCellValue('A'.($i + 1), $i);
                        $event->sheet->getDelegate()->setCellValue('B'.($i + 1), $student->std_nm);
                        if($student->std_gender == '1')
                            $event->sheet->getDelegate()->setCellValue('C'.($i + 1), "ប្រុស");
                        else
                            $event->sheet->getDelegate()->setCellValue('C'.($i + 1), "ស្រី");
                        $event->sheet->getDelegate()->setCellValue('D'.($i + 1), $student->std_total);
                        $event->sheet->getDelegate()->setCellValue('E'.($i + 1), $student->std_average);
                        $event->sheet->getDelegate()->setCellValue('F'.($i + 1), $i);
                        
                        //Convert Number to Text
                        $event->sheet->getStyle('A'.($i + 1))->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                        $event->sheet->getStyle('B'.($i + 1))->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                        $event->sheet->getStyle('C'.($i + 1))->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                        $event->sheet->getStyle('D'.($i + 1))->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                        $event->sheet->getStyle('E'.($i + 1))->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                        $event->sheet->getStyle('F'.($i + 1))->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                        $event->sheet->getStyle('G'.($i + 1))->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                        $event->sheet->getStyle('H'.($i + 1))->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                        $event->sheet->getStyle('I'.($i + 1))->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                        $event->sheet->getStyle('J'.($i + 1))->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                        $event->sheet->getStyle('K'.($i + 1))->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                        $event->sheet->getStyle('L'.($i + 1))->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                        
                        //Add Border for All Cells
                        $event->sheet->getStyle('A'.($i + 1))->applyFromArray($styleArr);
                        $event->sheet->getStyle('B'.($i + 1))->applyFromArray($styleArr);
                        $event->sheet->getStyle('C'.($i + 1))->applyFromArray($styleArr);
                        $event->sheet->getStyle('D'.($i + 1))->applyFromArray($styleArr);
                        $event->sheet->getStyle('E'.($i + 1))->applyFromArray($styleArr);
                        $event->sheet->getStyle('F'.($i + 1))->applyFromArray($styleArr);
                        $event->sheet->getStyle('G'.($i + 1))->applyFromArray($styleArr);
                        $event->sheet->getStyle('H'.($i + 1))->applyFromArray($styleArr);
                        $event->sheet->getStyle('I'.($i + 1))->applyFromArray($styleArr);
                        $event->sheet->getStyle('J'.($i + 1))->applyFromArray($styleArr);
                        $event->sheet->getStyle('K'.($i + 1))->applyFromArray($styleArr);
                        $event->sheet->getStyle('L'.($i + 1))->applyFromArray($styleArr);
                        
                        //Arrange cells
                        $event->sheet->getStyle('A'.($i + 1))->applyFromArray($center);
                        $event->sheet->getStyle('C'.($i + 1))->applyFromArray($center);
                        $event->sheet->getStyle('D'.($i + 1))->applyFromArray($center);
                        $event->sheet->getStyle('E'.($i + 1))->applyFromArray($center);
                        $event->sheet->getStyle('F'.($i + 1))->applyFromArray($center);
                        $event->sheet->getStyle('G'.($i + 1))->applyFromArray($center);
                        $event->sheet->getStyle('I'.($i + 1))->applyFromArray($center);
                        $event->sheet->getStyle('J'.($i + 1))->applyFromArray($center);
                        $event->sheet->getStyle('K'.($i + 1))->applyFromArray($center);
                        $event->sheet->getStyle('L'.($i + 1))->applyFromArray($center);
                    }
                    else if($i > 5)
                    {
                        $event->sheet->getDelegate()->setCellValue('G'.($j + 1), $i);
                        $event->sheet->getDelegate()->setCellValue('H'.($j + 1), $student->std_nm);
                        if($student->std_gender == '1')
                            $event->sheet->getDelegate()->setCellValue('C'.($j + 1), "ប្រុស");
                        else
                            $event->sheet->getDelegate()->setCellValue('C'.($j + 1), "ស្រី");
                        $event->sheet->getDelegate()->setCellValue('J'.($j + 1), $student->std_total);
                        $event->sheet->getDelegate()->setCellValue('K'.($j + 1), $student->std_average);
                        $event->sheet->getDelegate()->setCellValue('L'.($j + 1), $i);
                        $j++;
                    }
                    
                    //Set Size
                    $event->sheet->getColumnDimension('A')->setWidth(5);
                    $event->sheet->getColumnDimension('B')->setWidth(25);
                    $event->sheet->getColumnDimension('C')->setWidth(5);
                    $event->sheet->getColumnDimension('D')->setWidth(10);
                    $event->sheet->getColumnDimension('E')->setWidth(10);
                    $event->sheet->getColumnDimension('F')->setWidth(10);
                    $event->sheet->getColumnDimension('G')->setWidth(5);
                    $event->sheet->getColumnDimension('H')->setWidth(25);
                    $event->sheet->getColumnDimension('I')->setWidth(5);
                    $event->sheet->getColumnDimension('J')->setWidth(10);
                    $event->sheet->getColumnDimension('K')->setWidth(10);
                    $event->sheet->getColumnDimension('L')->setWidth(10);
                }
        }
        ];
    }
    
    private function formatDateTime($date)
    {
        return date('d-m-Y h:i:s A', strtotime($date));
    }
    
    private function formatDate($date)
    {
        return date('d-m-Y', strtotime($date));
    }
    
    private function formatTime($time)
    {
        return date('H:i', strtotime($time));
    }


}