<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Events\AfterSheet;
use PHPExcel_Style_Border;
use PHPExcel_Style_NumberFormat;
use App\StudentModel;
use PhpOffice\PhpSpreadsheet\Style\Alignment;

class StudentExport implements FromCollection, WithHeadings, WithEvents
{
    use Exportable;
    private $param = null;
    private $row_cnt = 0;
    
    public function __construct($param)
    {
        $this->param = $param;
    }
    
    public function headings(): array
    {
        return [
            'លេខរៀង',
            'ឈ្មោះ',
            'ភេទ',
            'ថ្ងៃខែឆ្នាំកំណើត',
            'គណិត',
            'រូប',
            'គីមី',
            'ជីវ',
            'ភាសាខ្មែរ',
            'គេហ',
            'ប្រវត្តិ',
            'ភូមិ',
            'ពលរដ្ឋ',
            'ផែនដី',
            'អប់រំកាយ',
            'អង់គ្លេស',
            'សិល្បៈ',
            'បំណិន',
            'កសិកម្ម',
            'ពិន្ទុសរុប',
            'មធ្យមភាគ',
            'ចំណាត់ថ្នាក់'
        ];
    }
    
    public function registerEvents(): array
    {
        $styleArr= [
            'borders' => [
                'outline' => [
                    'borderStyle' => PHPExcel_Style_Border::BORDER_THIN,
                ] ,
            ],
            
            'font' => [
                'name' => 'Khmer OS Battambang',
                'size' => 10
            ],
            
            'alignment' => [
                'vertical' => Alignment::VERTICAL_CENTER,
            ],
        ];
        
        $bold = [
            'font' => [
                'bold' => true
            ],
            
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
                'vertical' => Alignment::VERTICAL_CENTER,
            ],
        ];
        
        return [
            AfterSheet::class=>function(AfterSheet $event) use ($styleArr,$bold)
            {
                $event->sheet->getStyle('A1:V1')->applyFromArray($bold);
                $event->sheet->getDelegate()->getPageSetup()->setHorizontalCentered(true);
                $event->sheet->getDelegate()->getPageSetup()->setPaperSize(\PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
                $event->sheet->getDelegate()->getPageMargins()->setTop(1.3);
                $event->sheet->getDelegate()->getPageMargins()->setBottom(0.75);
                $event->sheet->getDelegate()->getPageMargins()->setLeft(0.25);
                $event->sheet->getDelegate()->getPageMargins()->setRight(0.25);
                $event->sheet->getDelegate()->getPageMargins()->setHeader(0.3);
                $event->sheet->getDelegate()->getPageMargins()->setFooter(0.3);
                
                $event->sheet->getDelegate()->getPageSetup()->setFitToWidth(1);
                //                 $event->sheet->getDelegate()->getHeaderFooter()->setOddHeader("This is font testing.");
                $event->sheet->getDelegate()->getHeaderFooter()->setOddHeader("&L&\"Khmer M1, -\"\nមន្ទីរអប់រំយុវជន និងកីឡារាជធានីភ្នំពេញ \nវិទ្យាល័យ កំបូល &C&\"Khmer M1, -\"\n\n\nបញ្ជីពិន្ទុសិស្សប្រចាំខែធ្នូ ឆ្នាំសិក្សា ២០១៩ ២០២០ \nថ្នាក់ទី ៧ង &R&\"Khmer M1, -\"ព្រះរាជាណាចក្រកម្ពុជា \nជាតិ សាសនា ព្រះមហាក្សត្រ");
                
                for($i = 1; $i <= $this->row_cnt +1; $i++)
                {
                    //Convert Number to Text
                    $event->sheet->getStyle('A'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                    $event->sheet->getStyle('B'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                    $event->sheet->getStyle('C'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                    $event->sheet->getStyle('D'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                    $event->sheet->getStyle('E'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                    $event->sheet->getStyle('F'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                    $event->sheet->getStyle('G'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                    $event->sheet->getStyle('H'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                    $event->sheet->getStyle('I'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                    $event->sheet->getStyle('J'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                    $event->sheet->getStyle('K'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                    $event->sheet->getStyle('L'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                    $event->sheet->getStyle('M'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                    $event->sheet->getStyle('N'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                    $event->sheet->getStyle('O'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                    $event->sheet->getStyle('P'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                    $event->sheet->getStyle('Q'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                    $event->sheet->getStyle('R'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                    $event->sheet->getStyle('S'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                    $event->sheet->getStyle('T'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                    $event->sheet->getStyle('U'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                    $event->sheet->getStyle('V'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                    
                    //Add Border for All Cells
                    $event->sheet->getStyle('A'.$i)->applyFromArray($styleArr);
                    $event->sheet->getStyle('B'.$i)->applyFromArray($styleArr);
                    $event->sheet->getStyle('C'.$i)->applyFromArray($styleArr);
                    $event->sheet->getStyle('D'.$i)->applyFromArray($styleArr);
                    $event->sheet->getStyle('E'.$i)->applyFromArray($styleArr);
                    $event->sheet->getStyle('F'.$i)->applyFromArray($styleArr);
                    $event->sheet->getStyle('G'.$i)->applyFromArray($styleArr);
                    $event->sheet->getStyle('H'.$i)->applyFromArray($styleArr);
                    $event->sheet->getStyle('I'.$i)->applyFromArray($styleArr);
                    $event->sheet->getStyle('J'.$i)->applyFromArray($styleArr);
                    $event->sheet->getStyle('K'.$i)->applyFromArray($styleArr);
                    $event->sheet->getStyle('L'.$i)->applyFromArray($styleArr);
                    $event->sheet->getStyle('M'.$i)->applyFromArray($styleArr);
                    $event->sheet->getStyle('N'.$i)->applyFromArray($styleArr);
                    $event->sheet->getStyle('O'.$i)->applyFromArray($styleArr);
                    $event->sheet->getStyle('P'.$i)->applyFromArray($styleArr);
                    $event->sheet->getStyle('Q'.$i)->applyFromArray($styleArr);
                    $event->sheet->getStyle('R'.$i)->applyFromArray($styleArr);
                    $event->sheet->getStyle('S'.$i)->applyFromArray($styleArr);
                    $event->sheet->getStyle('T'.$i)->applyFromArray($styleArr);
                    $event->sheet->getStyle('U'.$i)->applyFromArray($styleArr);
                    $event->sheet->getStyle('V'.$i)->applyFromArray($styleArr);
                    
                    //Set Size
                    $event->sheet->getColumnDimension('A')->setWidth(8);
                    $event->sheet->getColumnDimension('B')->setWidth(25);
                    $event->sheet->getColumnDimension('C')->setWidth(5);
                    $event->sheet->getColumnDimension('D')->setWidth(15);
                    $event->sheet->getColumnDimension('E')->setWidth(8);
                    $event->sheet->getColumnDimension('F')->setWidth(8);
                    $event->sheet->getColumnDimension('G')->setWidth(8);
                    $event->sheet->getColumnDimension('H')->setWidth(8);
                    $event->sheet->getColumnDimension('I')->setWidth(8);
                    $event->sheet->getColumnDimension('J')->setWidth(8);
                    $event->sheet->getColumnDimension('K')->setWidth(8);
                    $event->sheet->getColumnDimension('L')->setWidth(8);
                    $event->sheet->getColumnDimension('M')->setWidth(8);
                    $event->sheet->getColumnDimension('N')->setWidth(8);
                    $event->sheet->getColumnDimension('O')->setWidth(8);
                    $event->sheet->getColumnDimension('P')->setWidth(8);
                    $event->sheet->getColumnDimension('Q')->setWidth(8);
                    $event->sheet->getColumnDimension('R')->setWidth(8);
                    $event->sheet->getColumnDimension('S')->setWidth(9);
                    $event->sheet->getColumnDimension('T')->setWidth(9);
                    $event->sheet->getColumnDimension('U')->setWidth(9);
                    $event->sheet->getColumnDimension('V')->setWidth(9);
                }
        }
        ];
    }
    
    public function collection()
    {
        $columns = array();
        //array_push($columns, "no", "std_nm", "std_gender", "std_dob", "std_father_name", "std_father_job", "std_father_phone", "std_mother_name", "std_mother_job", "std_mother_phone", "std_birth_village", "std_birth_commune", "std_birth_district", "std_birth_province", "std_now_village", "std_now_commune", "std_now_district", "std_now_province");
        array_push($columns, "no", "std_nm", "std_gender", "std_dob");
        
        $students = new StudentModel();
        $result = collect($students->get_students_list(null, $this->param->CLS_ID))->sortBy('std_nm');
        
        $set_columns = array();
        $i = 0;
        foreach($result as $student)
        {
            $set_item = array();
            foreach($columns as $column)
            {
                if($column == "no")
                {
                    $set_item[$column] = $this->row_cnt + 1;
                }
                else
                {
                    if($column == "std_gender")
                    {
                        if($student->$column == "1"){
                            $set_item[$column] = "ប្រុស";
                        }
                        else {
                            $set_item[$column] = "ស្រី";
                        }
                    }
                    //                     else if(strpos($column, "in") > 0 || strpos($column, "out") > 0 || $column == "late" || $column == "early" || $column == "ot_tm")
                        //                     {
                        //                         if(empty($student->$column))
                            //                             $set_item[$column] = "N/A";
                            //                         else
                                //                             $set_item[$column] = $this->formatTime($student->$column);
                                //                     }
                    else
                    {
                        $set_item[$column] = $student->$column;
                    }
                }
            }
            
            array_push($set_columns, (object) $set_item);
            $this->row_cnt++;
        }
        
        return collect($set_columns);
    }
    
    private function formatDateTime($date)
    {
        return date('d-m-Y h:i:s A', strtotime($date));
    }
    
    private function formatDate($date)
    {
        return date('d-m-Y', strtotime($date));
    }
    
    private function formatTime($time)
    {
        return date('H:i', strtotime($time));
    }
}