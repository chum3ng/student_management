<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Events\AfterSheet;
use PHPExcel_Style_Border;
use PHPExcel_Style_NumberFormat;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use App\ScoreModel;
use Carbon\Carbon;

class GradeExport implements WithHeadings, WithEvents//FromCollection, 
{
    use Exportable;
    private $param = null;
    private $row_cnt = 0;
    private $month = "";
    
    public function __construct($param)
    {
        $this->param = $param;
    }
    
    public function headings(): array
    {
        return [
            'ល.រ',
            'ឈ្មោះ',
            'ភេទ',
            'ពិន្ទុសរុប',
            'មធ្យមភាគ',
            'ចំណាត់ថ្នាក់',
            'ល.រ',
            'ឈ្មោះ',
            'ភេទ',
            'ពិន្ទុសរុប',
            'មធ្យមភាគ',
            'ចំណាត់ថ្នាក់'
        ];
    }
    
    public function registerEvents(): array
    {
        $styleArr= [
            'borders' => [
                'outline' => [
                    'borderStyle' => PHPExcel_Style_Border::BORDER_THIN,
                ] ,
            ],
            
            'font' => [
                'name' => 'Khmer OS Battambang',
                'size' => 9
            ],
            
            'alignment' => [
                'vertical' => Alignment::VERTICAL_CENTER,
            ],
        ];
        
        $fontStyle = [           
            'font' => [
                'name' => 'Khmer OS Battambang',
                'size' => 9
            ],
            
            'alignment' => [
                'vertical' => Alignment::VERTICAL_TOP,
            ],
        ];
        
        $bold = [
            'font' => [
                'bold' => true
            ],
            
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
                'vertical' => Alignment::VERTICAL_CENTER,
            ],
        ];
        
        $center = [
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
                'vertical' => Alignment::VERTICAL_CENTER,
            ],
        ];
        
        $footer = [
            'font' => [
                'name' => 'Khmer OS Battambang',
                'size' => 9
            ],
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
                'vertical' => Alignment::VERTICAL_TOP,
            ],
        ];
        
        return [
            AfterSheet::class=>function(AfterSheet $event) use ($styleArr,$bold, $center, $fontStyle, $footer)
            {
                $grade = new GradeExport(null);
                
                if($this->param->MONTH_SCORE == '1') $this->month = "មករា";
                else if($this->param->MONTH_SCORE == '2') $this->month = "កុម្ភះ";
                else if($this->param->MONTH_SCORE == '3') $this->month = "មិនា";
                else if($this->param->MONTH_SCORE == '4') $this->month = "មេសា";
                else if($this->param->MONTH_SCORE == '5') $this->month = "ឧសភា";
                else if($this->param->MONTH_SCORE == '6') $this->month = "មិថុនា";
                else if($this->param->MONTH_SCORE == '7') $this->month = "កក្កដា";
                else if($this->param->MONTH_SCORE == '8') $this->month = "សីហា";
                else if($this->param->MONTH_SCORE == '9') $this->month = "កញ្ញា";
                else if($this->param->MONTH_SCORE == '10') $this->month = "តុលា";
                else if($this->param->MONTH_SCORE == '11') $this->month = "វិច្ចិកា";
                else if($this->param->MONTH_SCORE == '12') $this->month = "ធ្នូ";
                else if($this->param->MONTH_SCORE == '13') $this->month = "ឆមាសទី១";
                else if($this->param->MONTH_SCORE == '14') $this->month = "ឆមាសទី២";
                
                //For Header
                $event->sheet->getStyle('A1:L1')->applyFromArray($bold);
                //Convert Number to Text
                
                $col = 'A';
                
                for($i = 1; $i <= 12; $i++){
                    $event->sheet->getStyle($col.'1')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                    $event->sheet->getStyle($col.'1')->applyFromArray($styleArr);
                    $col = chr(ord($col) + 1);
                }
                
                //Arrange cells
                $event->sheet->getStyle('A1')->applyFromArray($center);
                $event->sheet->getStyle('C1')->applyFromArray($center);
                $event->sheet->getStyle('D1')->applyFromArray($center);
                $event->sheet->getStyle('E1')->applyFromArray($center);
                $event->sheet->getStyle('F1')->applyFromArray($center);
                $event->sheet->getStyle('G1')->applyFromArray($center);
                $event->sheet->getStyle('I1')->applyFromArray($center);
                $event->sheet->getStyle('J1')->applyFromArray($center);
                $event->sheet->getStyle('K1')->applyFromArray($center);
                $event->sheet->getStyle('L1')->applyFromArray($center);
                //End Header
                
                $event->sheet->getDelegate()->getPageSetup()->setHorizontalCentered(true);
                $event->sheet->getDelegate()->getPageSetup()->setPaperSize(\PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
                $event->sheet->getDelegate()->getPageMargins()->setTop(1.7);
//                 $event->sheet->getDelegate()->getPageMargins()->setBottom(2);
                $event->sheet->getDelegate()->getPageMargins()->setLeft(0.25);
                $event->sheet->getDelegate()->getPageMargins()->setRight(0.25);
                $event->sheet->getDelegate()->getPageMargins()->setHeader(0.3);
                $event->sheet->getDelegate()->getPageMargins()->setFooter(0.3);
                
                $event->sheet->getDelegate()->getPageSetup()->setFitToWidth(1);
                $event->sheet->getDelegate()->getHeaderFooter()->setOddHeader("&L&\"Khmer M1, -\"\nមន្ទីរអប់រំយុវជន និងកីឡារាជធានីភ្នំពេញ \nវិទ្យាល័យ កំបូល &C&\"Khmer M1, -\"\n\n\nបញ្ជីពិន្ទុសិស្សប្រចាំខែ" . $grade->getKhmerMonth($this->param->MONTH_SCORE) . " ឆ្នាំសិក្សា " . $this->param->YEAR . " \nថ្នាក់ទី " . $this->param->CLS_NM . " &R&\"Khmer M1, -\"ព្រះរាជាណាចក្រកម្ពុជា \nជាតិ សាសនា ព្រះមហាក្សត្រ");
                
                for($i = 1; $i <= 100; $i++){
                    $event->sheet->getDelegate()->getRowDimension($i)->setRowHeight(19.5);
                }
                
                //Score Object
                $scores = new ScoreModel();

//                 print_r($scores->get_students_with_rank($this->param, "std_average", "DESC"));
//                 exit();
                $result = collect($scores->get_students_with_rank($this->param, "std_average", "DESC"));
                $total = $scores->get_passed_avg($this->param, null, null, null);
                $girl_total = $scores->get_passed_avg($this->param, '0', null, null);
                $passed = $scores->get_passed_avg($this->param, null, '>=', null);
                $girl_passed = $scores->get_passed_avg($this->param, '0', '>=', null);
                $failed = $scores->get_passed_avg($this->param, null, null, '<');
                $girl_failed = $scores->get_passed_avg($this->param, '0', null, '<');
                
                //Footer
                $event->sheet->getDelegate()->mergeCells("A37:D46");
                $event->sheet->getDelegate()->mergeCells("E37:G46");
                $event->sheet->getDelegate()->mergeCells("H37:L46");
                
                //Footer Left
                $leftStr = "សិស្សសរុបចំនួន " . $grade->getKhmerNumber($total) . " នាក់ ស្រី " . $grade->getKhmerNumber($girl_total) . " នាក់ \nជាប់មធ្យមភាគ " . $grade->getKhmerNumber($passed) . " នាក់ ស្រី " . $grade->getKhmerNumber($girl_passed) . " នាក់\nធ្លាក់មធ្យមភាគ " . $grade->getKhmerNumber($failed) . " នាក់ ស្រី " . $grade->getKhmerNumber($girl_failed) . " នាក់";
                $event->sheet->getDelegate()->setCellValue('A37', $leftStr);
                
                //Footer Center
                $event->sheet->getDelegate()->setCellValue('E37', "\n\n\n\n\nបានឃើញ និង ឯកភាព\nនាយក");
                
                //Footer Right
                $currentDate = Carbon::now();
                
                $rightStr = "\n\n\nថ្ងៃ " . $grade->getKhmerDayofWeek($currentDate->dayOfWeek) . " ទី " . $grade->getKhmerNumber($currentDate->day) . " ខែ " . $grade->getKhmerMonth($currentDate->month) . " ឆ្នាំ " . $grade->getKhmerNumber($currentDate->year) . " \n  គ្រូបន្ទុកថ្នាក់";
                $event->sheet->getDelegate()->setCellValue('H37', $rightStr);
                
                $event->sheet->getDelegate()->getStyle('A37')->getAlignment()->setWrapText(true);
                $event->sheet->getDelegate()->getStyle('E37')->getAlignment()->setWrapText(true);
                $event->sheet->getDelegate()->getStyle('H37')->getAlignment()->setWrapText(true);
                
                $event->sheet->getStyle('A37')->applyFromArray($fontStyle);
                $event->sheet->getStyle('E37')->applyFromArray($footer);
                $event->sheet->getStyle('H37')->applyFromArray($footer);
             
                $i = 0;
                $j = 1;
                
                for($i = 0; $i < $result->count(); $i++)
                {

                    if(($i + 1) <= 34)
                    {
                        $event->sheet->getDelegate()->setCellValue('A'.($i + 2), $i + 1);
                        $event->sheet->getDelegate()->setCellValue('B'.($i + 2), $result[$i]->std_nm);
                        if($result[$i]->std_gender == '1')
                            $event->sheet->getDelegate()->setCellValue('C'.($i + 2), "ប្រុស");
                        else
                            $event->sheet->getDelegate()->setCellValue('C'.($i + 2), "ស្រី");
                        $event->sheet->getDelegate()->setCellValue('D'.($i + 2), $result[$i]->std_total);
                        $event->sheet->getDelegate()->setCellValue('E'.($i + 2), $result[$i]->std_average);
                        $event->sheet->getDelegate()->setCellValue('F'.($i + 2), $result[$i]->std_rank);
                                
                        //Convert Number to Text
                        $event->sheet->getStyle('A'.($i + 2))->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                        $event->sheet->getStyle('B'.($i + 2))->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                        $event->sheet->getStyle('C'.($i + 2))->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                        $event->sheet->getStyle('D'.($i + 2))->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                        $event->sheet->getStyle('E'.($i + 2))->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                        $event->sheet->getStyle('F'.($i + 2))->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                        $event->sheet->getStyle('G'.($i + 2))->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                        $event->sheet->getStyle('H'.($i + 2))->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                        $event->sheet->getStyle('I'.($i + 2))->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                        $event->sheet->getStyle('J'.($i + 2))->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                        $event->sheet->getStyle('K'.($i + 2))->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                        $event->sheet->getStyle('L'.($i + 2))->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
    
                        //Add Border for All Cells
                        $event->sheet->getStyle('A'.($i + 2))->applyFromArray($styleArr);
                        $event->sheet->getStyle('B'.($i + 2))->applyFromArray($styleArr);
                        $event->sheet->getStyle('C'.($i + 2))->applyFromArray($styleArr);
                        $event->sheet->getStyle('D'.($i + 2))->applyFromArray($styleArr);
                        $event->sheet->getStyle('E'.($i + 2))->applyFromArray($styleArr);
                        $event->sheet->getStyle('F'.($i + 2))->applyFromArray($styleArr);
                        $event->sheet->getStyle('G'.($i + 2))->applyFromArray($styleArr);
                        $event->sheet->getStyle('H'.($i + 2))->applyFromArray($styleArr);
                        $event->sheet->getStyle('I'.($i + 2))->applyFromArray($styleArr);
                        $event->sheet->getStyle('J'.($i + 2))->applyFromArray($styleArr);
                        $event->sheet->getStyle('K'.($i + 2))->applyFromArray($styleArr);
                        $event->sheet->getStyle('L'.($i + 2))->applyFromArray($styleArr);
    
                        //Arrange cells
                        $event->sheet->getStyle('A'.($i + 2))->applyFromArray($center);
                        $event->sheet->getStyle('C'.($i + 2))->applyFromArray($center);
                        $event->sheet->getStyle('D'.($i + 2))->applyFromArray($center);
                        $event->sheet->getStyle('E'.($i + 2))->applyFromArray($center);
                        $event->sheet->getStyle('F'.($i + 2))->applyFromArray($center);
                        $event->sheet->getStyle('G'.($i + 2))->applyFromArray($center);
                        $event->sheet->getStyle('I'.($i + 2))->applyFromArray($center);
                        $event->sheet->getStyle('J'.($i + 2))->applyFromArray($center);
                        $event->sheet->getStyle('K'.($i + 2))->applyFromArray($center);
                        $event->sheet->getStyle('L'.($i + 2))->applyFromArray($center);
                    }
                    else if(($i + 1) > 34)
                    {
                        $event->sheet->getDelegate()->setCellValue('G'.($j + 1), $i + 1);
                        $event->sheet->getDelegate()->setCellValue('H'.($j + 1), $result[$i]->std_nm);
                        if($result[$i]->std_gender == '1')
                            $event->sheet->getDelegate()->setCellValue('I'.($j + 1), "ប្រុស");
                        else
                            $event->sheet->getDelegate()->setCellValue('I'.($j + 1), "ស្រី");
                        $event->sheet->getDelegate()->setCellValue('J'.($j + 1), $result[$i]->std_total);
                        $event->sheet->getDelegate()->setCellValue('K'.($j + 1), $result[$i]->std_average);
                        $event->sheet->getDelegate()->setCellValue('L'.($j + 1), $result[$i]->std_rank);
                        $j++;
                   }

                        //Set Size
                        $event->sheet->getColumnDimension('A')->setWidth(5);
                        $event->sheet->getColumnDimension('B')->setWidth(25);
                        $event->sheet->getColumnDimension('C')->setWidth(5);
                        $event->sheet->getColumnDimension('D')->setWidth(10);
                        $event->sheet->getColumnDimension('E')->setWidth(10);
                        $event->sheet->getColumnDimension('F')->setWidth(10);
                        $event->sheet->getColumnDimension('G')->setWidth(5);
                        $event->sheet->getColumnDimension('H')->setWidth(25);
                        $event->sheet->getColumnDimension('I')->setWidth(5);
                        $event->sheet->getColumnDimension('J')->setWidth(10);
                        $event->sheet->getColumnDimension('K')->setWidth(10);
                        $event->sheet->getColumnDimension('L')->setWidth(10);
                    }
            }
                
//                 foreach($result as $student)
//                 {
//                     $i++;          
                    
// //                     echo ($result[7]->std_average);
// //                     exit();
                    
//                     if($i <= 30)
//                     {
//                         $event->sheet->getDelegate()->setCellValue('A'.($i + 1), $i);
//                         $event->sheet->getDelegate()->setCellValue('B'.($i + 1), $student->std_nm);
//                         if($student->std_gender == '1')
//                             $event->sheet->getDelegate()->setCellValue('C'.($i + 1), "ប្រុស");
//                         else
//                             $event->sheet->getDelegate()->setCellValue('C'.($i + 1), "ស្រី");
//                         $event->sheet->getDelegate()->setCellValue('D'.($i + 1), $student->std_total);
//                         $event->sheet->getDelegate()->setCellValue('E'.($i + 1), $student->std_average);
                        
//                         if()
//                         $event->sheet->getDelegate()->setCellValue('F'.($i + 1), $i);
                        
//                         //Convert Number to Text
//                         $event->sheet->getStyle('A'.($i + 1))->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
//                         $event->sheet->getStyle('B'.($i + 1))->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
//                         $event->sheet->getStyle('C'.($i + 1))->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
//                         $event->sheet->getStyle('D'.($i + 1))->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
//                         $event->sheet->getStyle('E'.($i + 1))->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
//                         $event->sheet->getStyle('F'.($i + 1))->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
//                         $event->sheet->getStyle('G'.($i + 1))->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
//                         $event->sheet->getStyle('H'.($i + 1))->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
//                         $event->sheet->getStyle('I'.($i + 1))->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
//                         $event->sheet->getStyle('J'.($i + 1))->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
//                         $event->sheet->getStyle('K'.($i + 1))->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
//                         $event->sheet->getStyle('L'.($i + 1))->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                        
//                         //Add Border for All Cells
//                         $event->sheet->getStyle('A'.($i + 1))->applyFromArray($styleArr);
//                         $event->sheet->getStyle('B'.($i + 1))->applyFromArray($styleArr);
//                         $event->sheet->getStyle('C'.($i + 1))->applyFromArray($styleArr);
//                         $event->sheet->getStyle('D'.($i + 1))->applyFromArray($styleArr);
//                         $event->sheet->getStyle('E'.($i + 1))->applyFromArray($styleArr);
//                         $event->sheet->getStyle('F'.($i + 1))->applyFromArray($styleArr);
//                         $event->sheet->getStyle('G'.($i + 1))->applyFromArray($styleArr);
//                         $event->sheet->getStyle('H'.($i + 1))->applyFromArray($styleArr);
//                         $event->sheet->getStyle('I'.($i + 1))->applyFromArray($styleArr);
//                         $event->sheet->getStyle('J'.($i + 1))->applyFromArray($styleArr);
//                         $event->sheet->getStyle('K'.($i + 1))->applyFromArray($styleArr);
//                         $event->sheet->getStyle('L'.($i + 1))->applyFromArray($styleArr);
                        
//                         //Arrange cells
//                         $event->sheet->getStyle('A'.($i + 1))->applyFromArray($center);
//                         $event->sheet->getStyle('C'.($i + 1))->applyFromArray($center);
//                         $event->sheet->getStyle('D'.($i + 1))->applyFromArray($center);
//                         $event->sheet->getStyle('E'.($i + 1))->applyFromArray($center);
//                         $event->sheet->getStyle('F'.($i + 1))->applyFromArray($center);
//                         $event->sheet->getStyle('G'.($i + 1))->applyFromArray($center);
//                         $event->sheet->getStyle('I'.($i + 1))->applyFromArray($center);
//                         $event->sheet->getStyle('J'.($i + 1))->applyFromArray($center);
//                         $event->sheet->getStyle('K'.($i + 1))->applyFromArray($center);
//                         $event->sheet->getStyle('L'.($i + 1))->applyFromArray($center);
//                     }
//                     else if($i > 30)
//                     {
//                         $event->sheet->getDelegate()->setCellValue('G'.($j + 1), $i);
//                         $event->sheet->getDelegate()->setCellValue('H'.($j + 1), $student->std_nm);
//                         if($student->std_gender == '1')
//                             $event->sheet->getDelegate()->setCellValue('I'.($j + 1), "ប្រុស");
//                         else
//                             $event->sheet->getDelegate()->setCellValue('I'.($j + 1), "ស្រី");
//                         $event->sheet->getDelegate()->setCellValue('J'.($j + 1), $student->std_total);
//                         $event->sheet->getDelegate()->setCellValue('K'.($j + 1), $student->std_average);
//                         $event->sheet->getDelegate()->setCellValue('L'.($j + 1), $i);
//                         $j++;
//                     }
                    
//                     //Set Size
//                     $event->sheet->getColumnDimension('A')->setWidth(5);
//                     $event->sheet->getColumnDimension('B')->setWidth(25);
//                     $event->sheet->getColumnDimension('C')->setWidth(5);
//                     $event->sheet->getColumnDimension('D')->setWidth(10);
//                     $event->sheet->getColumnDimension('E')->setWidth(10);
//                     $event->sheet->getColumnDimension('F')->setWidth(10);
//                     $event->sheet->getColumnDimension('G')->setWidth(5);
//                     $event->sheet->getColumnDimension('H')->setWidth(25);
//                     $event->sheet->getColumnDimension('I')->setWidth(5);
//                     $event->sheet->getColumnDimension('J')->setWidth(10);
//                     $event->sheet->getColumnDimension('K')->setWidth(10);
//                     $event->sheet->getColumnDimension('L')->setWidth(10);
//                 }
//         }
        ];
    }
    
    private function formatDateTime($date)
    {
        return date('d-m-Y h:i:s A', strtotime($date));
    }
    
    private function formatDate($date)
    {
        return date('d-m-Y', strtotime($date));
    }
    
    private function formatTime($time)
    {
        return date('H:i', strtotime($time));
    }
    
    public function getKhmerDayofWeek($dayOfWeek)
    {
        $khDayofWeek = '';
        if($dayOfWeek == 0) $khDayofWeek = "អាទិត្យ";
        else if($dayOfWeek == 1) $khDayofWeek = "ច័ន្ទ";
        else if($dayOfWeek == 2) $khDayofWeek = "អង្គារ"; 
        else if($dayOfWeek == 3) $khDayofWeek = "ពុធ"; 
        else if($dayOfWeek == 4) $khDayofWeek = "ព្រហស្បតិ៍"; 
        else if($dayOfWeek == 5) $khDayofWeek = "សុក្រ"; 
        else if($dayOfWeek == 6) $khDayofWeek = "សៅរ៏"; 
        
        return $khDayofWeek;
    }

    public function getKhmerNumber($number)
    {
        $strNum = strval($number);
        $result = "";
        for($i = 0; $i < strlen($strNum); $i++){
            $ch = substr($strNum, $i, 1);
            $khChar = '';
            
            if($ch == '0') $khChar = '០';
            else if($ch == '1') $khChar = '១';
            else if($ch == '2') $khChar = '២';
            else if($ch == '3') $khChar = '៣';
            else if($ch == '4') $khChar = '៤';
            else if($ch == '5') $khChar = '៥';
            else if($ch == '6') $khChar = '៦';
            else if($ch == '7') $khChar = '៧';
            else if($ch == '8') $khChar = '៨';
            else if($ch == '9') $khChar = '៩';
            
            $result = $result . $khChar;
        }
        
        return $result;
    }
    
    public function getKhmerMonth($month)
    {
        $khmerMonth = "មករា";
        if($month == 1) $khmerMonth = "មករា";
        else if($month == 2) $khmerMonth = "កុម្ភះ";
        else if($month == 3) $khmerMonth = "មីនា";
        else if($month == 4) $khmerMonth = "មេសា";
        else if($month == 5) $khmerMonth = "ឧសភា";
        else if($month == 6) $khmerMonth = "មិថុនា";
        else if($month == 7) $khmerMonth = "កក្កដា";
        else if($month == 8) $khmerMonth = "សីហា";
        else if($month == 9) $khmerMonth = "កញ្ញា";
        else if($month == 10) $khmerMonth = "តុលា";
        else if($month == 11) $khmerMonth = "វិច្ចកា";
        else if($month == 12) $khmerMonth = "ធ្នូ";
        return $khmerMonth;
    }
    
}