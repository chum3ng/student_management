<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ClassModel extends Model
{
    public function __contruct(){
        
    }
    
    public function get_classes_list($request = null){
        return DB::table('sms_classes')->select()->get();
    }
    
    public function save_class($request = null){
        $id = DB::table('sms_classes')->insertGetId(
                [
                    'class_nm' => $request->CLASS_NM,
                    'year' => $request->YEAR,
                ]
            );
        
        return $id;
    }
    
    public function get_class_by_id($request = null){
        return DB::table('sms_classes')->select()->where('id', '=', $request->ID)->get();
    }
    
    public function update_class($request = null){
        $result = DB::table('sms_classes')
        ->where('id', $request->ID)
        ->update(
            [
                'class_nm' => $request->CLASS_NM,
                'year' => $request->YEAR,
            ]
        );
        
        return $result;
    }
}
