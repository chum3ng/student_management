<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ClassController@index');
Route::get('save_cls', 'ClassController@save');
Route::get('get_cls_by_id', 'ClassController@get_cls_by_id');
Route::get('update_cls', 'ClassController@update_cls');

Route::get('save_std', 'StudentController@save');
Route::get('student/{id}', 'ClassController@request_students_list');
Route::get('get_std_by_id', 'StudentController@get_std_by_id');
Route::get('update_std', 'StudentController@update');

Route::get('dwl_export', 'ExportController@export');
Route::get('score_export', 'ExportController@exportScore');
Route::get('grade_export', 'ExportController@exportGrade');
Route::get('loyal_export', 'ExportController@exportLoyal');

Route::get('score', 'ScoreController@index');
Route::get('save_score', 'ScoreController@save');
Route::get('update_score', 'ScoreController@update');
