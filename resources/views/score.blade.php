<!DOCTYPE html>
<html>
<title>Add Students</title>
<?php echo view('includes/html_head')?>
<body>
    <form class="form-text">
        <div class="form-group text-center">
            <label>បញ្ចូលព៌តមានពិន្ទុរបស់សិស្ស</label>
        </div>

		<div class="form-row arrange">
            <div class="form-row col-sm-3">
                <label for="std-father-name" class="col-sm-4 col-form-label">ប្រចាំខែ:</label>
                <div class="col-sm-8">
                    <select class="form-control form-control" id="month-score">
                        <option value="1">មករា </option>
                        <option value="2">កុម្ភះ </option>
                        <option value="3">មិនា </option>
                        <option value="4">មេសា </option>
                        <option value="5">ឧសភា </option>
                        <option value="6">មិថុនា </option>
                        <option value="7">កក្កដា </option>
                        <option value="8">សីហា </option>
                        <option value="9">កញ្ញា </option>
                        <option value="10">តុលា </option>
                        <option value="11">វិច្ចកា </option>
                        <option value="12">ធ្នូ </option>
                    </select>
                </div>
            </div>
            
            <div class="form-row col-sm-3">
                <label for="std-father-job" class="col-sm-4 col-form-label">ប្រចាំឆ្នាំ:</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control form-control" id="year-score">
                </div>
            </div>
        </div>

        <div class="form-row arrange">
            <div class="form-row col-sm-3">
                <label for="std-father-name" class="col-sm-4 col-form-label">ឈ្មោះ:</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control form-control" id="std-name">
                </div>
            </div>
            
            <div class="form-row col-sm-3">
                <label for="std-father-job" class="col-sm-4 col-form-label">ថ្នាក់:</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control form-control" id="std-class">
                </div>
            </div>

            <div class="form-row col-sm-3">
                <label for="std-father-phone" class="col-sm-4 col-form-label">ឆ្នាំសិក្សា:</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control form-control" id="std-year">
                </div>
            </div>

            <div class="form-row col-sm-3">
                <input type="button" value="Search" class="btn btn-primary" id="btn-search" style="width:60%; margin: 0px 0px auto 50px;">
            </div>
        </div>

        <!-- <hr> -->
        <div class="table-responsive-lg table-container">
            <table class="table">
                <thead>
                    <tr>
                        <th>លេខរៀង</th>
                        <th>ឈ្មោះ</th>
                        <th>ថ្នាក់</th>
                        <th>ឆ្នាំសិក្សា</th>
                        <th>គណិត</th>
                        <th>រូប</th>
                        <th>គីមី</th>
                        <th>ជីវៈ</th>
                        <th>ខ្មែរ</th>
                        <th>ភូមិ</th>
                        <th>ប្រវត្តិ</th>
                        <th>ពលរដ្ឋ</th>
                        <th>គេហៈ</th>
                        <th>ផែនដី</th>
                        <th>អង់គ្លេស</th>
                        <th>សិល្បះ</th>
                        <th>សរុប</th>
                        <th>មធ្យមភាគ</th>
                    </tr>
                </thead>
                <tbody id = "std-list">
                   @foreach($stds as $no => $std)
    				<tr class="data-rows">
    					<td>{{ $no + 1 }}</td>
    					<td class="data-search" style="display: none;">{{ $std->id }}</td>
    					<td class="score-id" style="display: none;">{{ $std->scr_id }}</td>
    					<td>{{ $std->std_nm }}</td>
    					<td>{{ $std->class_nm }}</td>
    					<td>{{ $std->year }}</td>
    					<td>{{ $std->std_math }}</td>
    					<td>{{ $std->std_physic }}</td>
    					<td>{{ $std->std_chemistry }}</td>
    					<td>{{ $std->std_biology }}</td>
    					<td>{{ $std->std_khmer }}</td>
    					<td>{{ $std->std_geography }}</td>
    					<td>{{ $std->std_history }}</td>
    					<td>{{ $std->std_citizen }}</td>
    					<td>{{ $std->std_home }}</td>
    					<td>{{ $std->std_science }}</td>
    					<td>{{ $std->std_english }}</td>
    					<td>{{ $std->std_art }}</td>
    					<td>{{ $std->std_total }}</td>
    					<td>{{ $std->std_average }}</td>
    				</tr>
    				@endforeach
            	</tbody>
            </table>
        </div>

        <hr>
        <div class="form-row arrange input-form">
            <div class="form-row col-sm-3">
                    <label for="std-mother-name" class="col-sm-4 col-form-label">គណិតវិទ្យា:</label>
                <div class="col-sm-8">
                	<input hidden="true" autocomplete="off" type="input" id="scr-id" name="" placeholder="">
					<input hidden="true" autocomplete="off" type="input" id="std-id" name="" placeholder="">
                    <input type="text" class="form-control form-control" id="std-math">
                </div>
            </div>
            
            <div class="form-row col-sm-3">
                    <label for="std-mother-job" class="col-sm-4 col-form-label">រូបវិទ្យា:</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control form-control" id="std-physic">
                </div>
            </div>

            <div class="form-row col-sm-3">
                    <label for="std-mother-phone" class="col-sm-4 col-form-label">គីមីវិទ្យា:</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control form-control" id="std-chemistry">
                </div>
            </div>

            <div class="form-row col-sm-3">
                    <label for="std-mother-phone" class="col-sm-4 col-form-label">ជីវវិទ្យា:</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control form-control" id="std-biology">
                </div>
            </div>
        </div>

        <div class="form-row arrange input-form">
            <div class="form-row col-sm-3">
                <label for="std-birth-village" class="col-sm-4 col-form-label">ភាសាខ្មែរ:</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control form-control" id="std-khmer">
                </div>
            </div>
            
            <div class="form-row col-sm-3">
                <label for="std-birth-commune" class="col-sm-4 col-form-label">គេហៈវិទ្យា:</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control form-control" id="std-home">
                </div>
            </div>

            <div class="form-row col-sm-3">
                <label for="std-birth-district" class="col-sm-4 col-form-label">ពលរដ្ឋវិជ្ជា:</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control form-control" id="std-citizen">
                </div>
            </div>

            <div class="form-row col-sm-3">
                <label for="std-birth-province" class="col-sm-4 col-form-label">ប្រវត្តិវិទ្យា:</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control form-control" id="std-history">
                </div>
            </div>
        </div>

        <div class="form-row arrange input-form">
            <div class="form-row col-sm-3">
                <label for="std-now-village" class="col-sm-4 col-form-label">ភូមិវិទ្យា:</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control form-control" id="std-geography">
                </div>
            </div>
            
            <div class="form-row col-sm-3">
                <label for="std-now-commune" class="col-sm-4 col-form-label">ផែនដីវិទ្យា:</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control form-control" id="std-science">
                </div>
            </div>

            <div class="form-row col-sm-3">
                <label for="std-now-district" class="col-sm-4 col-form-label">អង់គ្លេស:</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control form-control" id="std-english">
                </div>
            </div>

            <div class="form-row col-sm-3">
                <label for="std-now-province" class="col-sm-4 col-form-label">អប់រំកាយ:</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control form-control" id="std-exercise">
                </div>
            </div>
        </div>

        <div class="form-row arrange input-form">
            <div class="form-row col-sm-3">
                <label for="std-now-village" class="col-sm-4 col-form-label">សិល្បៈ:</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control form-control" id="std-art">
                </div>
            </div>
            
            <div class="form-row col-sm-3">
                <label for="std-now-commune" class="col-sm-4 col-form-label">បំណិន:</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control form-control" id="std-lifelesson">
                </div>
            </div>
            
            <div class="form-row col-sm-6">
            	<div class="col-sm-4">
                	<input type="button" value="Submit" class="btn btn-primary" id="btn-submit"​​​​ style="width:60%; margin: 0px 0px auto 50px;">
                </div>
                <div class="col-sm-4">
                	<input type="button" value="Update" class="btn btn-primary" id="btn-update"​​​​ style="width:60%; margin: 0px 0px auto 50px;">
                </div>
                <div class="col-sm-4">
	                <a href="" class="btn btn-primary" id="btn-export" style="width:60%; margin: 0px 0px auto 50px;"> <i aria-hidden="true"></i>Export</a>
	            </div>
            </div>
            
<!--             <div class="form-row col-sm-3"> 
        		<a href="" class="btn btn-primary" id="btn-export" style="width:60%; margin: 0px 0px auto 50px;"> <i aria-hidden="true"></i>Export</a>
                 </div> -->
        </div>
    </form>
    
    <script id="std-list-tmpl" type="text/x-jquery-tmpl">
		<tr class="data-rows">
			<td>@{{= no}}</td>
            <td class="data-search" style="display: none;">@{{= id}}</td>
            <td class="score-id" style="display: none;">@{{= scr_id}}</td>
			<td>@{{= std_nm}}</td>
			<td>@{{= class_nm}}</td>
			<td>@{{= year}}</td>
            <td>@{{= std_math}}</td>
			<td>@{{= std_physic}}</td>
			<td>@{{= std_chemistry}}</td>
            <td>@{{= std_biology}}</td>
			<td>@{{= std_khmer}}</td>
			<td>@{{= std_geography}}</td>
            <td>@{{= std_history}}</td>
			<td>@{{= std_citizen}}</td>
			<td>@{{= std_home}}</td>
            <td>@{{= std_science}}</td>
			<td>@{{= std_english}}</td>
			<td>@{{= std_art}}</td>
            <td>@{{= std_total}}</td>
			<td>@{{= std_average}}</td>
		</tr>
	</script>
    
	<?php echo view('includes/html_tail')?>
	<script type="text/javascript">
		var ajax_stds = "{{ URL::to('score') }}";
		var save_score = "{{ URL::to('save_score') }}";
		var update_score = "{{ URL::to('update_score') }}";
	</script>
	{!! Html::script('resources/assets/js/score.js?'.date('Ymdhis')) !!}
</body>
</html>
