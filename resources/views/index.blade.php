<!DOCTYPE html>
<html>
<title>Add Students</title>
<?php echo view('includes/html_head')?>
<body>
    <form class="form-input">
        <div class="form-group text-center">
            <label>បញ្ចូលព៌តមានរបស់សិស្ស</label>
<!--             <input hidden="true" autocomplete="off" type="text" id="class-id" name="" placeholder="" value="{{ $ID }}"> -->
			<label hidden="true" id="class-id">{{ $ID }}</label>
        </div>

        <div class="form-row arrange">
            <div class="form-row col-sm-4">
                <label for="std-name" class="col-sm-4 col-form-label">ឈ្មោះ:</label>
                <div class="col-sm-8">
                	<input type="input" class="form-control form-control" id="id" style="display: none;">
                    <input type="text" class="form-control form-control" id="std-name">
                </div>
            </div>
            
            <div class="form-row col-sm-4">
                <label for="std-dob" class="col-sm-4 col-form-label">ថ្ងៃខែឆ្នាំកំណើត:</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control form-control" id="std-dob">
                </div>
            </div>

            <div class="form-row col-sm-4">
                <label for="std-gender" class="col-sm-4 col-form-label">ភេទ: </label>
                <div class="col-sm-8">
                    <select class="form-control form-control" id="std-gender">
                        <option value="1">ប្រុស </option>
                        <option value="0">ស្រី </option>
                    </select>
                </div>
            </div>
        </div>

        <div class="form-row arrange">
            <div class="form-row col-sm-4">
                <label for="std-father-name" class="col-sm-4 col-form-label">ឈ្មោះឱពុក: </label>
                <div class="col-sm-8">
                    <input type="text" class="form-control form-control" id="std-father-name">
                </div>
            </div>
            
            <div class="form-row col-sm-4">
                <label for="std-father-job" class="col-sm-4 col-form-label">មុខរបរ: </label>
                <div class="col-sm-8">
                    <input type="text" class="form-control form-control" id="std-father-job">
                </div>
            </div>

            <div class="form-row col-sm-4">
                <label for="std-father-phone" class="col-sm-4 col-form-label">លេខទូរសព្ទ: </label>
                <div class="col-sm-8">
                    <input type="text" class="form-control form-control" id="std-father-phone">
                </div>
            </div>
        </div>

        <div class="form-row arrange">
            <div class="form-row col-sm-4">
                    <label for="std-mother-name" class="col-sm-4 col-form-label">ឈ្មោះម្តាយ: </label>
                <div class="col-sm-8">
                    <input type="text" class="form-control form-control" id="std-mother-name">
                </div>
            </div>
            
            <div class="form-row col-sm-4">
                    <label for="std-mother-job" class="col-sm-4 col-form-label">មុខរបរ: </label>
                <div class="col-sm-8">
                    <input type="text" class="form-control form-control" id="std-mother-job">
                </div>
            </div>

            <div class="form-row col-sm-4">
                    <label for="std-mother-phone" class="col-sm-4 col-form-label">លេខទូរសព្ទ: </label>
                <div class="col-sm-8">
                    <input type="text" class="form-control form-control" id="std-mother-phone">
                </div>
            </div>
        </div>

        <div class="form-row arrange" style="font-weight: bold;">កន្លែងកំណើត </div>
        <hr>
        <div class="form-row arrange">
            <div class="form-row col-sm-3">
                <label for="std-birth-village" class="col-sm-4 col-form-label">ភូមិ: </label>
                <div class="col-sm-8">
                    <input type="text" class="form-control form-control" id="std-birth-village">
                </div>
            </div>
            
            <div class="form-row col-sm-3">
                <label for="std-birth-commune" class="col-sm-4 col-form-label">សង្កាត់: </label>
                <div class="col-sm-8">
                    <input type="text" class="form-control form-control" id="std-birth-commune">
                </div>
            </div>

            <div class="form-row col-sm-3">
                <label for="std-birth-district" class="col-sm-4 col-form-label">ខណ្ឌ: </label>
                <div class="col-sm-8">
                    <input type="text" class="form-control form-control" id="std-birth-district">
                </div>
            </div>

            <div class="form-row col-sm-3">
                <label for="std-birth-province" class="col-sm-4 col-form-label">រាជធានី: </label>
                <div class="col-sm-8">
                    <input type="text" class="form-control form-control" id="std-birth-province">
                </div>
            </div>
        </div>

        <div class="form-row arrange" style="font-weight: bold;">ទីលំនៅបច្ចុប្បន្ន </div>
        <hr>
        <div class="form-row arrange">
            <div class="form-row col-sm-3">
                <label for="std-now-village" class="col-sm-4 col-form-label">ភូមិ: </label>
                <div class="col-sm-8">
                    <input type="text" class="form-control form-control" id="std-now-village">
                </div>
            </div>
            
            <div class="form-row col-sm-3">
                <label for="std-now-commune" class="col-sm-4 col-form-label">សង្កាត់: </label>
                <div class="col-sm-8">
                    <input type="text" class="form-control form-control" id="std-now-commune">
                </div>
            </div>

            <div class="form-row col-sm-3">
                <label for="std-now-district" class="col-sm-4 col-form-label">ខណ្ឌ: </label>
                <div class="col-sm-8">
                    <input type="input" class="form-control form-control" id="std-now-district" value="កំបូល">
                </div>
            </div>

            <div class="form-row col-sm-3">
                <label for="std-now-province" class="col-sm-4 col-form-label">រាជធានី: </label>
                <div class="col-sm-8">
                    <input type="input" class="form-control form-control" id="std-now-province"​ value="ភ្នំពេញ">
                </div>
            </div>
        </div>


		<input type="button" value="Submit" class="btn btn-primary" id="btn-submit">
		<a href="" class="btn btn-primary" id="btn-export"> <i aria-hidden="true"></i>Export</a>
		<label id="result" style="color:red;"></label>
<!--         <button class="btn btn-primary" id="btn-submit">Submit</button> -->
<!--         <button class="btn btn-primary" id="btn-export">Export</button> -->
    </form>

    <div class="table-responsive-lg table-container">
        <table class="table">
            <thead>
                <tr>
                    <th>លេខរៀង </th>
                    <th>ឈ្មោះ </th> 
                    <th>ភេទ </th>
                    <th>ថ្ងៃខែឆ្នាំកំណើត </th>
                    <th>ឈ្មោះឱពុក </th>
                    <th>ឈ្មោះម្តាយ </th>
                </tr>
            </thead>
            <tbody id = "std-list">
               @foreach($students as $no => $std)
				<tr class="data-rows">
					<td>{{ $no + 1 }}</td>
					<td class="data-search" style="display: none;">{{ $std->id }}</td>
					<td>{{ $std->std_nm }}</td>
					@if($std->std_gender == 1)
						<td>ប្រុស </td>
					@else
						<td>ស្រី </td>
					@endif
					<td>{{ $std->std_dob }}</td>
					<td>{{ $std->std_father_name }}</td>
					<td>{{ $std->std_mother_name }}</td>
				</tr>
				@endforeach
            </tbody>
        </table>
    </div>
    
    <script id="std-list-tmpl" type="text/x-jquery-tmpl">
		<tr class="data-rows">
			<td>@{{= no}}</td>
            <td class="data-search" style="display: none;">@{{= id}}</td>
			<td>@{{= std_nm}}</td>
			<td>@{{= std_gender}}</td>
			<td>@{{= std_dob}}</td>
            <td>@{{= std_father_name}}</td>
            <td>@{{= std_mother_name}}</td>
		</tr>
	</script>
    
	<?php echo view('includes/html_tail')?>
	<script type="text/javascript">
		var ajax_std_list = "{{ URL::to('student') }}/{{ $ID }}";
		var save_std = "{{ URL::to('save_std') }}";
		var get_std_by_id = "{{ URL::to('get_std_by_id') }}";
		var update_std = "{{ URL::to('update_std') }}";
		var ajax_export = "{{ URL::to('dwl_export') }}";
	</script>
	{!! Html::script('resources/assets/js/students.js?'.date('Ymdhis')) !!}
</body>
</html>
