<!DOCTYPE html>
<html>
<title>Add Scores</title>
<?php echo view('includes/html_head')?>
<body>
    <form class="form-input">
    	<br>
        <div class="form-group text-center">
            <label>បញ្ចូលពិន្ទុរបស់សិស្ស</label>
        </div>
        
        <div class="form-row arrange" style="height: 480px;">
            <div class="col-md-6" style="border-right: 2px solid black;">
            	
            	<div class="form-row arrange">
                	<div class="form-row col-sm-4">
                            <label for="name-add" class="col-sm-4 col-form-label">ឈ្មោះ:</label>
                        <div class="col-sm-8">
                        	<input hidden="true" autocomplete="off" type="input" id="scr-id" name="" placeholder="">
                        	<input hidden="true" autocomplete="off" type="input" id="std-id" name="" placeholder="">
                            <input type="text" class="form-control form-control" id="name-add">
                        </div>
                    </div>
                </div>
            
                <div class="form-row arrange">
                    <div class="form-row col-sm-4">
                        <label for="month-score" class="col-sm-4 col-form-label">ប្រចាំខែ:</label>
                        <div class="col-sm-8">
                            <select class="form-control form-control" id="month-score">
                                <option value="1">មករា </option>
                                <option value="2">កុម្ភះ </option>
                                <option value="3">មិនា </option>
                                <option value="4">មេសា </option>
                                <option value="5">ឧសភា </option>
                                <option value="6">មិថុនា </option>
                                <option value="7">កក្កដា </option>
                                <option value="8">សីហា </option>
                                <option value="9">កញ្ញា </option>
                                <option value="10">តុលា </option>
                                <option value="11">វិច្ចកា </option>
                                <option value="12">ធ្នូ </option>
                                <option value="13">ឆមាសទី ១</option>
                                <option value="14">ឆមាសទី ២</option>
                            </select>
                        </div>
                    </div>
                    
                    <div class="form-row col-sm-4">
                            <label for="std-khmer" class="col-sm-4 col-form-label">ខ្មែរ:</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control form-control" id="std-khmer">
                        </div>
                    </div>
                    
                    <div class="form-row col-sm-4">
                            <label for="std-citizen" class="col-sm-4 col-form-label">ពលរដ្ឋ:</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control form-control" id="std-citizen">
                        </div>
                    </div>  
                </div>

                <div class="form-row arrange">
                    <div class="form-row col-sm-4">
                            <label for="std-history" class="col-sm-4 col-form-label">ប្រវត្តិ:</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control form-control" id="std-history">
                        </div>
                    </div> 
                    
                    <div class="form-row col-sm-4">
                            <label for="std-geography" class="col-sm-4 col-form-label">ភូមិ:</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control form-control" id="std-geography">
                        </div>
                    </div>
                    
                    <div class="form-row col-sm-4">
                        <label for="std-math" class="col-sm-4 col-form-label">គណិត:</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control form-control" id="std-math">
                        </div>
                    </div>         
                </div>
        
                <div class="form-row arrange">
                    <div class="form-row col-sm-4">
                        <label for="std-physic" class="col-sm-4 col-form-label">រូប:</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control form-control" id="std-physic">
                        </div>
                    </div>
                    
                    <div class="form-row col-sm-4">
                        <label for="std-chemistry" class="col-sm-4 col-form-label">គីមី:</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control form-control" id="std-chemistry">
                        </div>
                    </div>
        
                    <div class="form-row col-sm-4">
                        <label for="std-biology" class="col-sm-4 col-form-label">ជីវ:</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control form-control" id="std-biology">
                        </div>
                    </div>
                </div>
        
                <div class="form-row arrange">
                    <div class="form-row col-sm-4">
                        <label for="std-science" class="col-sm-4 col-form-label">ផែនដី:</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control form-control" id="std-science">
                        </div>
                    </div>
                    
                    <div class="form-row col-sm-4">
                        <label for="std-english" class="col-sm-4 col-form-label">អង់គ្លេស:</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control form-control" id="std-english">
                        </div>
                    </div>
                    
                    <div class="form-row col-sm-4">
                        <label for="std-home" class="col-sm-4 col-form-label">គេហៈ:</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control form-control" id="std-home">
                        </div>
                    </div>
                </div>
        
                <div class="form-row arrange">
                	<div class="form-row col-sm-4">
                        <label for="std-art" class="col-sm-4 col-form-label">សិល្បៈ:</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control form-control" id="std-art">
                        </div>
                    </div>
                
                    <div class="form-row col-sm-4">
                        <label for="std-exercise" class="col-sm-4 col-form-label">អ.កាយ:</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control form-control" id="std-exercise">
                        </div>
                    </div>
                    
                    <div class="form-row col-sm-4">
                        <label for="std-lifelesson" class="col-sm-4 col-form-label">បំណិន:</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control form-control" id="std-lifelesson">
                        </div>
                    </div>
                </div>
        
                <div class="form-row arrange">
                	<div class="form-row col-sm-4">
                        <label for="std-agriculture" class="col-sm-4 col-form-label">កសិកម្ម:</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control form-control" id="std-agriculture">
                        </div>
                    </div>

					<div class="form-row col-sm-4">
                        <label for="std-dividend" class="col-sm-4 col-form-label">ផលចែក:</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control form-control" id="std-dividend">
                        </div>
                    </div>
                
                    <div class="form-row col-sm-4">
                        <label for="std-total" class="col-sm-4 col-form-label">សរុប:</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control form-control" id="std-total">
                        </div>
                    </div>
                </div>
        
                <div class="form-row arrange">
                	<div class="form-row col-sm-4">
                        <label for="std-average" class="col-sm-4 col-form-label">ម.ភាគ:</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control form-control" id="std-average">
                        </div>
                    </div>
                
                    <div class="form-row col-sm-4">
                        <input type="button" value="Submit" class="btn btn-primary" id="btn-submit"​​​​ style="width: 100%; margin: 0px 0px auto 50px;">
                    </div>
                    
                    <div class="form-row col-sm-4">
                        <a href="" class="btn btn-primary" id="btn-export" style="width: 100%; margin: 0px 0px auto 50px;"> <i aria-hidden="true"></i>តារាងគ្រប់មុខ</a>
                    </div>
                </div>
                
                <div class="form-row arrange">
                	<div class="form-row col-sm-4">
                        <a href="" class="btn btn-primary" id="btn-grade" style="width: 100%; margin: 0px 0px auto 50px;"> <i aria-hidden="true"></i>តារាងចំណាត់ថ្នាក់</a>
                    </div>
                    
                    <div class="form-row col-sm-4">
                        <a href="" class="btn btn-primary" id="btn-loyal" style="width: 100%; margin: 0px 0px auto 50px;"> <i aria-hidden="true"></i>តារាងកិត្តិយស</a>
                    </div>
                    
<!--                     <div class="form-row col-sm-4">
                        <a href="" class="btn btn-primary" id="btn" style="width: 100%; margin: 0px 0px auto 50px;"> <i aria-hidden="true"></i></a>
	                     </div> -->
                </div>

            </div>
            
            <div class="col-md-6" style="height: 100%;">
                <div class="form-row arrange">
                    <div class="form-row col-sm-4">
                        <label for="std-father-name" class="col-sm-4 col-form-label">ឈ្មោះ:</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control form-control" id="std-name">
                        </div>
                    </div>
                    
                    <div class="form-row col-sm-4">
                        <label for="std-father-job" class="col-sm-4 col-form-label">ថ្នាក់:</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control form-control" id="std-class">
                        </div>
                    </div>

                    <div class="form-row col-sm-4">
                        <label for="std-father-phone" class="col-sm-4 col-form-label">ឆ្នាំ:</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control form-control" id="std-year">
                        </div>
                    </div>

                </div>
                
                <div class="form-row arrange">
                	<div class="form-row col-sm-4">
                        <label for="month-search" class="col-sm-4 col-form-label">ប្រចាំខែ:</label>
                        <div class="col-sm-8">
                            <select class="form-control form-control" id="month-search">
                                <option value="1">មករា </option>
                                <option value="2">កុម្ភះ </option>
                                <option value="3">មិនា </option>
                                <option value="4">មេសា </option>
                                <option value="5">ឧសភា </option>
                                <option value="6">មិថុនា </option>
                                <option value="7">កក្កដា </option>
                                <option value="8">សីហា </option>
                                <option value="9">កញ្ញា </option>
                                <option value="10">តុលា </option>
                                <option value="11">វិច្ចកា </option>
                                <option value="12">ធ្នូ </option>
                                <option value="13">ឆមាសទី ១</option>
                                <option value="14">ឆមាសទី ២</option>
                            </select>
                        </div>
                    </div>
                    
                    <div class="form-row col-sm-4">
                    	<input type="button" value="Search" class="btn btn-primary" id="btn-search" style="width:100%; margin: 0px 0px auto 50px;">
                    </div>
         		</div>

				<div class="table-responsive-lg table-container" style="height: 290px;">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>លេខរៀង</th>
                                <th>ឈ្មោះ</th>
                                <th>ថ្នាក់</th>
                                <th>ឆ្នាំសិក្សា</th>
                                <th>បញ្ចូលរួច</th>
                            </tr>
                        </thead>
                        <tbody id="std-list">
                       		@foreach($stds as $no => $std)
            				<tr class="data-rows">
            					<td>{{ $no + 1 }}</td>
            					<td class="data-search" style="display: none;">{{ $std->id }}</td>
            					<td class="score-id" style="display: none;">{{ $std->scr_id }}</td>
            					<td class="name-cell">{{ $std->std_nm }}</td>
            					<td>{{ $std->class_nm }}</td>
            					<td>{{ $std->year }}</td>
            					@if(empty($std->scr_id))
            					<td>នៅទេ</td>
            					@else
            					<td>រួចរាល់</td>
            					@endif
            				</tr>
        					@endforeach
                		</tbody>
                    </table>
                </div>
            </div>
        </div>
    </form>
    
    <script id="std-list-tmpl" type="text/x-jquery-tmpl">
		<tr class="data-rows">
			<td>@{{= no}}</td>
            <td class="data-search" style="display: none;">@{{= id}}</td>
            <td class="score-id" style="display: none;">@{{= scr_id}}</td>
			<td class="name-cell">@{{= std_nm}}</td>
			<td>@{{= class_nm}}</td>
			<td>@{{= year}}</td>
            <td>@{{= status}}</td>
		</tr>
	</script>
    
	<?php echo view('includes/html_tail')?>
	<script type="text/javascript">
		var ajax_stds = "{{ URL::to('score') }}";
		var save_score = "{{ URL::to('save_score') }}";
		var update_score = "{{ URL::to('update_score') }}";
		var export_score = "{{ URL::to('score_export') }}";
		var export_grade = "{{ URL::to('grade_export') }}";
	</script>
	{!! Html::script('resources/assets/js/score1.js?'.date('Ymdhis')) !!}
</body>
</html>
