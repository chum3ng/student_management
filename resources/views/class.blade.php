<!DOCTYPE html>
<html>
<title>Add Classes</title>
<?php echo view('includes/html_head')?>
<body>
    <form class="form-input">
        <div class="form-group text-center">
            <label>បញ្ចូលព៌តមានរបស់ថ្នាក់</label>
        </div>
    
        <div class="form-row arrange">
            <div class="form-row col-sm-4">
                <label for="std-name" class="col-sm-4 col-form-label">ឈ្មោះថ្នាក់:</label>
                <div class="col-sm-8">
                	<input type="input" class="form-control form-control" id="id" style="display: none;">
                    <input type="text" class="form-control form-control" id="class-name">
                </div>
            </div>
            
            <div class="form-row col-sm-4">
                <label for="std-dob" class="col-sm-4 col-form-label">ឆ្នាំសិក្សា:</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control form-control" id="study-year">
                </div>
            </div>
    
        </div>
    
        <input type="button" value="Submit" class="btn btn-primary" id="btn-submit">
        <a href="{{ URL::to('score') }}" class="btn btn-primary" id="btn-score" style="width:100px;"> <i aria-hidden="true"></i>Add Score</a>
        <label id="result" style="color:red;"></label>
    </form>
    
    <div class="table-responsive-lg table-container">
        <table class="table">
            <thead>
                <tr>
                    <th>លេខរៀង</th>
                    <th>ឈ្មោះថ្នាក់</th>
                    <th>ឆ្នាំសិក្សា</th>
                </tr>
            </thead>
            <tbody id = "cls-list">
               @foreach($classes as $no => $cls)
				<tr class="data-rows">
					<td>{{ $no + 1 }}</td>
					<td class="data-search" style="display: none;">{{ $cls->id }}</td>
					<td><a href="{{URL::to('student')}}/{{$cls->id}}">{{ $cls->class_nm }}</a></td>
					<td>{{ $cls->year }}</td>
				</tr>
				@endforeach
            </tbody>
        </table>
    </div>
    
    <script id="cls-list-tmpl" type="text/x-jquery-tmpl">
		<tr class="data-rows">
			<td>@{{= no}}</td>
            <td class="data-search" style="display: none;">@{{= id}}</td>
			<td><a href="@{{= href }}">@{{= class_nm}}</a></td>
			<td>@{{= year}}</td>
		</tr>
	</script>
    
	<?php echo view('includes/html_tail')?>
	<script type="text/javascript">
		var ajax_cls_list = "{{ URL::to('/') }}";
		var save_cls = "{{ URL::to('save_cls') }}";
		var base_url = "{{ URL::to('student') }}";
		var get_cls_by_id = "{{ URL::to('get_cls_by_id') }}";
		var update_cls = "{{ URL::to('update_cls') }}";
	</script>
	{!! Html::script('resources/assets/js/class.js?'.date('Ymdhis')) !!}
</body>
</html>
