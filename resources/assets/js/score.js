
var score = {};
var isEdit = false;
var month = new Date().getMonth() + 1;
var year = new Date().getFullYear();

flex.extend(
{
	//TODO: Onload Js
	onload: function()
	{
		$(".input-form *").prop('disabled', true);
		
		var input = {
				"MONTH_SCORE": month,
				"YEAR_SCORE": year
		};
		
		score.get_data_list(input);
		$("#month-score").val(month);
		$("#year-score").val(year);
	},
	
	//TODO: On Event Js
	events: function()
	{
		$(document).on("click", "#btn-search", function(){
			var month_score = $("#month-score").val();
			var year_score = $("#year-score").val();
			var std_name = $("#std-name").val();
			var std_class = $("#std-class").val();
			var std_year = $("#std-year").val();
			
			var input = {
				"MONTH_SCORE": month_score,
				"YEAR_SCORE": year_score,
				"STD_NM": std_name,
				"CLS_NM": std_class,
				"YEAR": std_year
			};
			
			score.get_data_list(input);
		});
		
		$(document).on("dblclick", ".data-rows", function(){
			$(".input-form *").prop('disabled', false);
			var stdId = $(this).closest(".data-rows").find(".data-search").text();
			var scrId = $(this).closest(".data-rows").find(".score-id").text();
			$("#std-id").val(stdId);
			$("#scr-id").val(scrId);
			score.display_score_to_form(stdId);
			
			$("tr").removeClass('highlight');
			$(this).closest(".data-rows").addClass('highlight');
		});
		
		$(document).on("click", "#btn-submit", function(){
			var input = score.get_data_from_form();
			
			if(input["error"])
			{
				alert(input["msg"]);
				return;
			}
			if(input != 0){
				
				comm.request.createAjaxRequest(input, save_score, "GET", function(dat)
				{
					if(!dat.ERROR){
						score.get_data_list();
						alert(dat.MSG);
						$("input[type=text]").val("");
					}
					else
					{
						alert(dat.MSG);
					}
				}, false);
			}
		});
		
		$(document).on("click", "#btn-update", function(){
			var id = $("#scr-id").val();
			var input = score.get_data_from_form();
			input["ID"] = id;
			
			if(input["error"])
			{
				alert(input["msg"]);
				return;
			}

			comm.request.createAjaxRequest(input, update_score, "GET", function(dat){
				if(!dat.ERROR){
					score.get_data_list();
					alert(dat.MSG);
					$("input[type=text]").val("");
				}
				else
				{
					alert(dat.MSG);
				}
			}, false);
		});
	}
});

score.ajax_score_update = function()
{
	var id = $("#scr-id").val();
	var input = score.get_data_from_form();
	input["ID"] = id;
	
	console.log(input);
	
	if(input["error"])
	{
		alert(input["msg"]);
		return;
	}

	comm.request.createAjaxRequest(input, update_score, "GET", function(dat){
		if(!dat.ERROR){
			score.get_data_list();
			alert(dat.MSG);
			$("input[type=text]").val("");
		}
		else
		{
			alert(dat.MSG);
		}
	}, false);
}

score.display_score_to_form = function(id)
{
	var input = {
		"ID": id
	};
	
	comm.request.createAjaxRequest(input, ajax_stds, "GET", function(dat){
		$("#std-math").val(dat[0].std_math);
		$("#std-physic").val(dat[0].std_physic);
		$("#std-chemistry").val(dat[0].std_chemistry);
		$("#std-biology").val(dat[0].std_biology);
		$("#std-khmer").val(dat[0].std_khmer);
		$("#std-home").val(dat[0].std_home);
		$("#std-citizen").val(dat[0].std_citizen);
		$("#std-history").val(dat[0].std_history);
		$("#std-geography").val(dat[0].std_geography);
		$("#std-science").val(dat[0].std_science);
		$("#std-english").val(dat[0].std_english);
		$("#std-exercise").val(dat[0].std_exercise);
		$("#std-art").val(dat[0].std_art);
		$("#std-lifelesson").val(dat[0].std_lifelesson);
		
		if(dat[0].month_score == null)
			$("#month-score").val(month);
		else
			$("#month-score").val(dat[0].month_score);
		
		if(dat[0].year_score == null)
			$("#year-score").val(year);
		else
			$("#year-score").val(dat[0].year_score);
		
	}, false);
}

score.get_data_list = function(input){
	
	comm.request.createAjaxRequest(input, ajax_stds, "GET", function(dat)
	{
			$("#std-list").empty();
			
			$("tfoot").hide();
			
			$.map(dat, function(val, i){
				val["no"] = i + 1;
				return val;
			});
			
			$("#std-list-tmpl").tmpl(dat).appendTo("#std-list");
	}, false);
	
}

score.get_data_from_form = function(){
	var input = {
			"msg": "",
			"error": false
		};
		
		var std_id 		= $("#std-id").val();
		var month 		= $("#month-score").val();
		var year  		= $("#year-score").val();
		var math 		= $("#std-math").val();
		var physic  	= $("#std-physic").val();
		var chemistry 	= $("#std-chemistry").val();
		var biology 	= $("#std-biology").val();
		var khmer 		= $("#std-khmer").val();
		var home 		= $("#std-home").val();
		var citizen 	= $("#std-citizen").val();
		var history 	= $("#std-history").val();
		var geography 	= $("#std-geography").val();
		var science 	= $("#std-science").val();
		var english 	= $("#std-english").val();
		var exercise 	= $("#std-exercise").val();
		var art 		= $("#std-art").val();
		var lifelesson 	= $("#std-lifelesson").val();
		
		if(flex.isNull(month)){
			input["msg"] = "សូមបញ្ចូលប្រចាំខែ។";
			input["error"]  = true;
			$("#month-score").focus();
			return input;
		}
		if(flex.isNull(year)){
			input["msg"] = "សូមបញ្ចូលប្រចាំឆ្នាំ។";
			input["error"]  = true;
			$("#year-score").focus();
			return input;
		}
		
		var total = parseFloat(math) + parseFloat(physic) + parseFloat(chemistry) + parseFloat(biology) + parseFloat(khmer) + parseFloat(home) 
					+ parseFloat(citizen) + parseFloat(history) + parseFloat(geography) + parseFloat(science) + parseFloat(english) 
					+ parseFloat(exercise);
		
		var average = parseFloat(total) / 12;
		
		input = {
				'STD_ID'		: std_id,
				'MONTH_SCORE' 	: month,
				'YEAR_SCORE'  	: year,
				'MATH'  		: math,
				'PHYSIC'		: physic,
				'CHEMISTRY'		: chemistry,
				'BIOLOGY'		: biology,
				'KHMER'			: khmer,
				'HOME' 			: home,
				'CITIZEN'		: citizen,
				'HISTORY'		: history,
				'GEOGRAPHY'		: geography,
				'SCIENCE'		: science,
				'ENGLISH'		: english,
				'EXERCISE'		: exercise,
				'ART'			: art,
				'LIFELESSON'	: lifelesson,
				'TOTAL'			: total,
				'AVERAGE'		: average
		}
		
		return input;
}