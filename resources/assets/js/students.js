
var student = {};
var isEdit = false;

flex.extend(
{
	//TODO: Onload Js
	onload: function()
	{
		
	},
	
	//TODO: On Event Js
	events: function()
	{
		$(document).on("keypress", ".form-input", function(key){
			var keycode = (key.keyCode ? key.keyCode : key.which);
			if(keycode == '13'){
				$("#btn-submit").trigger("click").focus();	      
			}
		});
		
		$(document).on("click", "#btn-submit", function(){
			if(isEdit == true){
				student.ajax_student_update();
			}
			else
			{
				var input = student.get_student_from_form();
				
				if(input["error"])
				{
					alert(input["msg"]);
					return;
				}
				if(input != 0){
					
					comm.request.createAjaxRequest(input, save_std, "GET", function(dat)
					{
						if(!dat.ERROR){
							student.get_students_list();
							$("#result").text(dat.MSG);
							$("input[type=text]").val("");
						}
						else
						{
							$("#result").text(dat.MSG);
						}
					}, false);
				}
			}
		});
		
		$(document).on("dblclick", ".data-rows", function(){
			var stdId = $(this).closest(".data-rows").find(".data-search").text();
			student.display_student_to_form(stdId);
		});
		
		$(document).on("click", "#btn-export", function(){
			var class_id = $("#class-id").text();
			var input = {
				'CLS_ID': class_id	
			};
			$(this).attr("href", ajax_export+'?json='+encodeURIComponent(JSON.stringify(input)));
		});
	}
});

student.display_student_to_form = function(id)
{
	var input = {
		"ID": id
	};
	isEdit = true;
	
	comm.request.createAjaxRequest(input, get_std_by_id, "GET", function(dat){
		$("#id").val(dat[0].id);
		$("#std-name").val(dat[0].std_nm);
		$("#std-dob").val(dat[0].std_dob);
		$("#std-gender").val(dat[0].std_gender);
		$("#std-father-name").val(dat[0].std_father_name);
		$("#std-father-job").val(dat[0].std_father_job);
		$("#std-father-phone").val(dat[0].std_father_phone);
		$("#std-mother-name").val(dat[0].std_mother_name);
		$("#std-mother-job").val(dat[0].std_mother_job);
		$("#std-mother-phone").val(dat[0].std_mother_phone);
		$("#std-birth-village").val(dat[0].std_birth_village);
		$("#std-birth-commune").val(dat[0].std_birth_commune);
		$("#std-birth-district").val(dat[0].std_birth_district);
		$("#std-birth-province").val(dat[0].std_birth_province);
		$("#std-now-village").val(dat[0].std_now_village);
		$("#std-now-commune").val(dat[0].std_now_commune);
		$("#std-now-district").val(dat[0].std_now_district);
		$("#std-now-province").val(dat[0].std_now_province);
//		$("#class-id").val(dat[0].class_id);
	}, false);
}

student.ajax_student_update = function()
{
	var id = $("#id").val();
	var input = student.get_student_from_form();
	input["ID"] = id;
	
	if(input["error"])
	{
		alert(input["msg"]);
		return;
	}

	comm.request.createAjaxRequest(input, update_std, "GET", function(dat){
		if(!dat.ERROR){
			student.get_students_list();
			$("#result").text(dat.MSG);
			$("input[type=text]").val("");
			$("#id").val("");
			isEdit = false;
		}
		else
		{
			$("#result").text(dat.MSG);
		}
	}, false);
}

student.get_students_list = function()
{
//	var class_id = $("#class-id").val();
//	var input = {
//		'CLS_ID': class_id
//	};
	var input = "";
	comm.request.createAjaxRequest(input, ajax_std_list, "GET", function(dat)
	{
		$("#std-list").empty();
		
		$("tfoot").hide();
		
		$.map(dat, function(val, i){
			val["no"] = i + 1;
			if(val["std_gender"] == 1) val["std_gender"] = "ប្រុស";
			else val["std_gender"] = "ស្រី";
			return val;
		});
		
		$("#std-list-tmpl").tmpl(dat).appendTo("#std-list");
	}, false);
}

student.get_student_from_form = function()
{
	var input = {
		"msg": "",
		"error": false
	};
	
	var std_name 			= $("#std-name").val();
	var std_dob  			= $("#std-dob").val();
	var std_gender 			= $("#std-gender").val();
	var std_father_name 	= $("#std-father-name").val();
	var std_father_job 		= $("#std-father-job").val();
	var std_father_phone 	= $("#std-father-phone").val();
	var std_mother_name 	= $("#std-mother-name").val();
	var std_mother_job 		= $("#std-mother-job").val();
	var std_mother_phone 	= $("#std-mother-phone").val();
	var std_birth_village 	= $("#std-birth-village").val();
	var std_birth_commune 	= $("#std-birth-commune").val();
	var std_birth_district 	= $("#std-birth-district").val();
	var std_birth_province 	= $("#std-birth-province").val();
	var std_now_village 	= $("#std-now-village").val();
	var std_now_commune 	= $("#std-now-commune").val();
	var std_now_district 	= $("#std-now-district").val();
	var std_now_province 	= $("#std-now-province").val();
	var class_id			= $("#class-id").text();
	
	if(flex.isNull(std_name)){
		input["msg"] = "ឈ្មោះមិនត្រឹមត្រូវ។";
		input["error"]  = true;
		$("#std-name").focus();
		return input;
	}
	if(flex.isNull(std_dob)){
		input["msg"] = "ថ្ងៃខែឆ្នាំកំណើតមិនត្រឹមត្រូវ។";
		input["error"]  = true;
		$("#std-dob").focus();
		return input;
	}
	if(flex.isNull(std_gender)){
		input["msg"] = "ភេទមិនត្រឹមត្រូវ។";
		input["error"]  = true;
		$("#std-gender").focus();
		return input;
	}
	if(flex.isNull(std_father_name)){
		input["msg"] = "ឈ្មោះឱពុកមិនត្រឹមត្រូវ។";
		input["error"]  = true;
		$("#std-father-name").focus();
		return input;
	}
	if(flex.isNull(std_father_job)){
		input["msg"] = "មុខរបររបស់ឱពុកមិនត្រឹមត្រូវ។";
		input["error"]  = true;
		$("#std-father-job").focus();
		return input;
	}
	if(flex.isNull(std_mother_name)){
		input["msg"] = "ឈ្មោះម្តាយមិនត្រឹមត្រូវ។";
		input["error"]  = true;
		$("#std-mother-name").focus();
		return input;
	}
	if(flex.isNull(std_mother_job)){
		input["msg"] = "មុខរបររបស់ម្តាយមិនត្រឹមត្រូវ។";
		input["error"]  = true;
		$("#std-mother-job").focus();
		return input;
	}
	if(flex.isNull(std_birth_commune)){
		input["msg"] = "ឃុំកំណើតមិនត្រឹមត្រូវ។";
		input["error"]  = true;
		$("#std-birth-commune").focus();
		return input;
	}
	if(flex.isNull(std_birth_district)){
		input["msg"] = "ស្រុកកំណើតមិនត្រឹមត្រូវ។";
		input["error"]  = true;
		$("#std-birth-district").focus();
		return input;
	}
	if(flex.isNull(std_birth_province)){
		input["msg"] = "ខេត្តកំណើតមិនត្រឹមត្រូវ។";
		input["error"]  = true;
		$("#std-birth-province").focus();
		return input;
	}
	if(flex.isNull(std_now_commune)){
		input["msg"] = "ឃុំបច្ចុប្បន្នមិនត្រឹមត្រូវ។";
		input["error"]  = true;
		$("#std-now-commune").focus();
		return input;
	}
	if(flex.isNull(std_now_district)){
		input["msg"] = "ស្រុកបច្ចុប្បន្នមិនត្រឹមត្រូវ។";
		input["error"]  = true;
		$("#std-now-district").focus();
		return input;
	}
	if(flex.isNull(std_now_province)){
		input["msg"] = "ខេត្តបច្ចុប្បន្នមិនត្រឹមត្រូវ។";
		input["error"]  = true;
		$("#std-now-province").focus();
		return input;
	}
	
	input = {
			"STD_NAME": std_name,
			"STD_DOB" : std_dob,
			"STD_GENDER": std_gender,
			"FATHER_NAME": std_father_name,
			"FATHER_JOB": std_father_job,
			"FATHER_PHONE": std_father_phone,
			"MOTHER_NAME": std_mother_name,
			"MOTHER_JOB": std_mother_job,
			"MOTHER_PHONE": std_mother_phone,
			"BIRTH_VILLAGE": std_birth_village,
			"BIRTH_COMMUNE": std_birth_commune,
			"BIRTH_DISTRICT": std_birth_district,
			"BIRTH_PROVINCE": std_birth_province,
			"NOW_VILLAGE": std_now_village,
			"NOW_COMMUNE": std_now_commune,
			"NOW_DISTRICT": std_now_district,
			"NOW_PROVINCE": std_now_province,
			"CLS_ID": class_id
	}
	
	return input;
	
}