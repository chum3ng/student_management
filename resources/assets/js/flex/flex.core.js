var flex;
if(!flex) flex={};	

flex.callbackFn = {};
flex = new function() 
{
//	this.extend = function(input,callbackFn)
//	{
//		if(!input) input = {};
//		window.addEventListener("load", input['onload']);
//		$(document).ready(input['events']);
//	};\
	this.extend = function(input,callbackFn)
	 {
		  if(!input) input = {};
		  if (window.addEventListener) {
			   window.addEventListener("load", input['onload']);
		  }
		  else {
			  window.attachEvent("onload", input['onload']);
		  }
		  $(document).ready(input['events']);
	 };
	
	this.isNull = function(param){ return (param==undefined || param=="")?true:false; };
	
	this.debounce = function(func, wait, immediate) 
	{
		var timeout;
		return function() 
		{
			var context = this, args = arguments;
			var later = function() 
			{
				timeout = null;
				if (!immediate) func.apply(context, args);
			};
			
			var callNow = immediate && !timeout;
			clearTimeout(timeout);
			timeout = setTimeout(later, wait);
			
			if (callNow) func.apply(context, args);
		};
	};
};