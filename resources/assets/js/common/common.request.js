var comm;
if(!comm) comm={};	
if(!comm.request) 
{
	comm.request={};
	comm.request.createAjaxRequest = function(input, url, method, callbackFn, isLoading) 
	{
		if(isLoading == undefined) isLoading = true;
		if(isLoading) comm.ui.createProgressBar();
		
		if(!input) var input = {};
		
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});
		
		$.ajax({
			type     : method,
			data     : input,
			dataType : 'json',
			url      : url,
			cache    : false,
			success  : function(dat)
			{
				if(!flex.isNull(dat.url)) 
				{
					alert("Session is timed out.");
					window.location.replace(dat.url);
				}
				callbackFn(dat);
				if(isLoading) comm.ui.destroyProgressBar();
			}/*,
		    error: function(xhr, textStatus, error) {
		        alert("Problem calling: " + url + "\nCode: " + xhr.status + "\nException: " + textStatus);
		    }*/
		});
	}
}