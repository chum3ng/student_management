var comm;
if(!comm) comm={};	

if(!comm.ui) 
{
	comm.ui={};
	
	comm.ui.createProgressBar = function() 
	{
		var origin;
		if (!window.location.origin) 
		{
			origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port: '');
		}
		else
		{
			origin = window.location.origin;
		
		}
		var baseUrl = origin + '/kbas/'
				
		$("#gloading-container").remove();
		$("body").prepend("<div id='gloading-container'>" +
								"<div id='gloading-bacgnd'></div>" +
								"<div id='gloading-img'>" +
									"<h2>Loading...</h2>" +
									"<img src='"+baseUrl+"resources/assets/img/loading/loading_apple.gif' />" +
								"</div>" +
						  "</div>");
		$("body").css('cursor','wait');
	};	
	
	comm.ui.destroyProgressBar = function() 
	{
		$("#selectAll").prop('checked', false);
		$("#gloading-bacgnd,#gloading-img").fadeOut(100);
		$("body").css('cursor','default');
	};
	
	// create pagination
	comm.ui.createLinkPagination = function ( div_id, callback, curPageNo, totPage, pageSize) 
	{
		var fullTagOpen = '<div class="w3-bar w3-border w3-round">';
		var fullTagClose = '</div>';
		var firstHtml = fullTagOpen + '<a id="paging_first" class="w3-bar-item w3-button w3-disabled">First</a>';
		var prevHtml = '<a id="paging_prev" class="w3-bar-item w3-button w3-disabled">Prev</a>';
		var nextHtml = '<a id="paging_next" class="w3-bar-item w3-button w3-disabled">Next</a>';
		var lastHtml = '<a id="paging_last" class="w3-bar-item w3-button w3-disabled">Last</a>' + fullTagClose;
		var pageHtml = '';
		
		var page_size = 10;
		
		if(pageSize)
		{
			page_size = pageSize;
		}
		else
		{
			page_size = 10;
		}
		
		var currentPage  = (curPageNo)?curPageNo:1;
		if(parseInt(currentPage) < 1) currentPage = 1;
		if(parseInt(currentPage) > parseInt(totPage)) currentPage = totPage;
		var currentBlock = Math.ceil(currentPage/page_size);
		var firstPage     = currentBlock*page_size-page_size+1;
		var lastPage      = currentBlock*page_size;
		
		$("#"+div_id).children().remove();

		if(totPage > 0){
			
			var countNum=0;
			
			for(var i = firstPage; i <= lastPage && i <= totPage;  i++){
				
				if(currentPage == i){
					pageHtml += '<a class="w3-bar-item w3-button pag_num w3-blue w3-disabled">'+i+'</a>';
				}else{ 
					pageHtml += '<a class="w3-bar-item w3-button pag_num">'+i+'</a>';
				}
			} 	
			pageHtml += " </a>";
			
			$("#"+div_id).append(firstHtml+prevHtml+pageHtml+nextHtml+lastHtml);
			
			if(currentPage>10){ //enable first arrow // enable prev arrow
				$("#paging_first").removeClass("w3-disabled");
				$("#paging_prev").removeClass("w3-disabled");
			}
			
			//if(totPage-currentPage>=5){
			if(totPage-lastPage > 0){ // enable last arrow // enable next arrow 
				$("#paging_last").removeClass("w3-disabled");
				$("#paging_next").removeClass("w3-disabled");
			}
		}else{
			pageHtml += ' <a class="w3-bar-item w3-button pag_num w3-blue w3-disabled">1</a>';
			
			$("#"+div_id).append(firstHtml+prevHtml+pageHtml+nextHtml+lastHtml);
		}
		
		var input = {};
		$("#"+div_id).find("#paging_first").click(function(){
			
			if($(this).hasClass("w3-disabled")==true){ return false;}
			
			if($.isFunction(callback)){
				input["PG_NO"] = 1;
				callback(input);
			}
		});
		$("#"+div_id).find("#paging_prev").click(function(){
			
			if($(this).hasClass("w3-disabled")==true){ return false;}
			
			currentBlock--;
			currentPage = currentBlock*page_size-page_size+1;
			if(currentPage < 0) currentPage = 1;
			if($.isFunction(callback)){
				input["PG_NO"] = currentPage;
				callback(input);
			}
		});
		$("#"+div_id).find("#paging_next").click(function()
		{
			if($(this).hasClass("w3-disabled")==true){ return false;}
			
			currentBlock++;
			currentPage = currentBlock*page_size-page_size+1;
			
			
			if(currentPage > totPage){
				currentPage = totPage;
			}
			if($.isFunction(callback)){
				input["PG_NO"] = currentPage;
				callback(input);
			}
		});
		$("#"+div_id).find("#paging_last").click(function(){

			if($(this).hasClass("w3-disabled")==true){ return false;}
			
			if($.isFunction(callback)){
				input["PG_NO"] = totPage;
				callback(input);
			}
		});
		$("#"+div_id).find(".pag_num").click(function(){
			
			if($(this).hasClass("w3-disabled")==true){ return false;}
			
			currentPage = $(this).html();
			if($.isFunction(callback)){
				input["PG_NO"] = currentPage;
				callback(input);
			}
		});
	};
	
	comm.ui.generateNumbering = function(pgNo, pgSize, startNo)
	{
		return (startNo+1)+(Number(pgNo)*Number(pgSize))-Number(pgSize);
	};
	
	//Generate table columns dynamic
	comm.ui.generateTableColumnsDynamic = function(columns, table_selector, tbody_selector, option)
	{
		//define table tags
		var table_start_tag    = '<table class="table table-hover">';
		var table_end_tag      = '</table>';
		var colgroup_start_tag = '<colgroup>';
		var colgroup_end_tag   = '</colgroup>';
		var col_start_tag      = '<col style="width:';
		var col_end_tag        = ';" />';
		var tbody_start_tag    = '<tbody id="'+tbody_selector+'" class="list text-nowrap">';
		var tbody_end_tag      = '</tbody>';
		var thead_start_tag    = '<thead class="text-white text-nowrap">';
		var thead_end_tag      = '</thead>';
		var tfoot_start_tag    = '<tfoot style="display:none;">';
		var tfoot_end_tag      = '</tfoot>';
		var tr_th_start_tag    = '<tr>';
		var tr_th_end_tag      = '</tr>';
		var tr_start_tag       = '<tr v-for="(item, index) in items">';
		var tr_end_tag         = '</tr>';
		var th_start_tag       = '<th scope="col">';
		var th_end_tag         = '</th>';
		var td_start_tag       = '<td>';
		var td_end_tag         = '</td>';
		var col                = '';
		var th                 = '';
		var td                 = '';
		
		for(var i=0; i < columns.length; i++)
		{
			col += col_start_tag + columns[i].column_sz + col_end_tag;
			th  += th_start_tag  + columns[i].column_nm + th_end_tag;
			td  += td_start_tag  + '{{ item.'+columns[i].column_id+' }}' + td_end_tag;
		}
		
		//colgroup block
		colgroup_start_tag += col + colgroup_end_tag;
		
		//thead block
		thead_start_tag    += tr_th_start_tag + th + tr_th_end_tag + thead_end_tag;
		
		//tbody block
		if(option)
		{
			tbody_start_tag    += tr_start_tag + td + tr_end_tag + tbody_end_tag;
		}
		else
		{
			tbody_start_tag += tbody_end_tag;
		}
		
		//tfoot block
		tfoot_start_tag    += '<tr><td class="w3-center" colspan="'+columns.length+'">There is no content available</td></tr>' + tfoot_end_tag;
		
		//table block
		table_start_tag += colgroup_start_tag + thead_start_tag + tbody_start_tag + tfoot_start_tag + table_end_tag;
		
		//set to html
		$(table_selector).html(table_start_tag);
	};
	
	comm.ui.setFileUpload = function(formId, action,requestParam,callbackFn)
	{
		var url = '';
		
		var form = document.getElementById(formId);
		
		url = action+"?callbackFn="+callbackFn+"&"+requestParam;
			
		form.setAttribute("action", url);

		form.target ="postiframe";
		comm.ui.submitForm(form);
	};	
	
	comm.ui.submitForm = function(form){
		try {
			form.submit();
		} catch (e) {
			setTimeout(function () { comm.ui.submitForm(form); }, 50);
		}
	};
};