
var score = {};
var isEdit = false;
var isDblClick = false;
var month_score = new Date().getMonth() + 1;
var year_score = new Date().getFullYear();

flex.extend(
{
	//TODO: Onload Js
	onload: function()
	{	
		//Set default value for month-score & month-search
		$("#month-score").val(month_score);
		$("#month-search").val(month_score);
		
	},
	
	//TODO: On Event Js
	events: function()
	{
		$(document).on('click', '#btn-search', function(){
			score.ajax_search();
		});
		
		$(document).on('dblclick', '.data-rows', function(){
			isDblClick = true;
			var scrId = $(this).closest(".data-rows").find(".score-id").text();
			$("#name-add").val($(this).closest(".data-rows").find(".name-cell").text());
			$("#std-id").val($(this).closest(".data-rows").find(".data-search").text());
			$("#month-score").val($("#month-search").val());
			$("#scr-id").val(scrId);

			score.display_score_to_form(scrId);
		});
		
		$(document).on('click', '#btn-submit', function(){
			if(isDblClick == true){
				if(isEdit == false) {
					score.ajax_add_score();
				}
				else {
					score.ajax_update_score();
				}
			} else {
				alert("សូមចុច ២​ ដងលើតារាង។");
			}
		});
		
		$(document).on("click", "#btn-export", function(){
			var month_score = $("#month-search").val();
			var std_class = $("#std-class").val();
			var std_year = $("#std-year").val();
			
			if(std_class === "" || std_year === "") alert("សូមបញ្ចូល ថ្នាក់ និងឆ្នាំសិក្សា។")
			else {
				var input = {
					"MONTH_SCORE": month_score,
					"CLS_NM": std_class,
					"YEAR": std_year
				};
				
				$(this).attr("href", export_score +'?json='+encodeURIComponent(JSON.stringify(input)));
			}
		});
		
		$(document).on("click", "#btn-grade", function(){
			var month_score = $("#month-search").val();
			var std_class = $("#std-class").val();
			var std_year = $("#std-year").val();
			
			if(std_class === "" || std_year === "") alert("សូមបញ្ចូល ថ្នាក់ និងឆ្នាំសិក្សា។")
			else {
				var input = {
					"MONTH_SCORE": month_score,
					"CLS_NM": std_class,
					"YEAR": std_year
				};
				
				$(this).attr("href", export_grade +'?json='+encodeURIComponent(JSON.stringify(input)));
			}
		});
		
		$(document).on("click", "#btn-loyal", function(){
			alert("អត់ទាន់ហើយ");
//			var month_score = $("#month-search").val();
//			var std_class = $("#std-class").val();
//			var std_year = $("#std-year").val();
//			
//			if(std_class === "" || std_year === "") alert("សូមបញ្ចូល ថ្នាក់ និងឆ្នាំសិក្សា។")
//			else {
//				var input = {
//					"MONTH_SCORE": month_score,
//					"CLS_NM": std_class,
//					"YEAR": std_year
//				};
//				
//				$(this).attr("href", export_loyal +'?json='+encodeURIComponent(JSON.stringify(input)));
//			}
		});
	}
});

score.ajax_update_score = function()
{
	var id = $("#scr-id").val();
	var input = score.get_data_from_form();
	input["ID"] = id;
	var default_month = $("#month-search").val();
	var param = {
		"MONTH_SCORE": default_month
	};
	
	if(input["error"])
	{
		alert(input["msg"]);
		return;
	}

	comm.request.createAjaxRequest(input, update_score, "GET", function(dat){
		if(!dat.ERROR){
			score.get_data_list(param);
			alert(dat.MSG);
			$("input[type=text]").val("");
		}
		else
		{
			alert(dat.MSG);
		}
	}, false);
}

score.ajax_search = function()
{
	var month_score = $("#month-search").val();
	var std_class = $("#std-class").val();
	var std_year = $("#std-year").val();
	var std_name = $("#std-name").val();
	
	var input = {
		"MONTH_SCORE": month_score,
		"STD_NM": std_name,
		"CLS_NM": std_class,
		"YEAR": std_year
	};
	
	score.get_data_list(input);
};

score.ajax_add_score = function()
{
	var input = score.get_data_from_form();
	
	var default_month = $("#month-search").val();
	var param = {
		"MONTH_SCORE": default_month
	};
	
	if(input["error"])
	{
		alert(input["msg"]);
		return;
	}
	if(input != 0){
		
		comm.request.createAjaxRequest(input, save_score, "GET", function(dat)
		{
			if(!dat.ERROR){
				score.get_data_list(param);
				alert(dat.MSG);
				$("input[type=text]").val("");
			}
			else
			{
				alert(dat.MSG);
			}
		}, false);
	}
}

score.display_score_to_form = function(id)
{
	var input = {
		"ID": id
	};
	
	if(id === ""){
		isEdit = false;
		$("#std-math").val("0.00");
		$("#std-physic").val("0.00");
		$("#std-chemistry").val("0.00");
		$("#std-biology").val("0.00");
		$("#std-khmer").val("0.00");
		$("#std-home").val("0.00");
		$("#std-citizen").val("0.00");
		$("#std-history").val("0.00");
		$("#std-geography").val("0.00");
		$("#std-science").val("0.00");
		$("#std-english").val("0.00");
		$("#std-exercise").val("0.00");
		$("#std-art").val("0.00");
		$("#std-lifelesson").val("0.00");
		$("#std-agriculture").val("0.00");
		$("#std-dividend").val("0");
		$("#std-total").val("");
		$("#std-average").val("");
	} else {
		isEdit = true;
		comm.request.createAjaxRequest(input, ajax_stds, "GET", function(dat){
			$("#std-math").val(dat[0].std_math);
			$("#std-physic").val(dat[0].std_physic);
			$("#std-chemistry").val(dat[0].std_chemistry);
			$("#std-biology").val(dat[0].std_biology);
			$("#std-khmer").val(dat[0].std_khmer);
			$("#std-home").val(dat[0].std_home);
			$("#std-citizen").val(dat[0].std_citizen);
			$("#std-history").val(dat[0].std_history);
			$("#std-geography").val(dat[0].std_geography);
			$("#std-science").val(dat[0].std_science);
			$("#std-english").val(dat[0].std_english);
			$("#std-exercise").val(dat[0].std_exercise);
			$("#std-art").val(dat[0].std_art);
			$("#std-lifelesson").val(dat[0].std_lifelesson);
			$("#std-agriculture").val(dat[0].std_agriculture);
			$("#std-total").val(dat[0].std_total);
			$("#std-dividend").val(dat[0].dividend);
			$("#std-average").val(dat[0].std_average);
			
			if(dat[0].month_score == null)
				$("#month-score").val(month_score);
		
		}, false);
	}
	
}

score.get_data_list = function(input){   
	
	comm.request.createAjaxRequest(input, ajax_stds, "GET", function(dat)
	{
			$("#std-list").empty();
			
			$("tfoot").hide();
			
			$.map(dat, function(val, i){
				val["no"] = i + 1;
				val["status"] = flex.isNull(val["scr_id"]) ? "នៅទេ" : "រួចរាល់";
				return val;
			});
			
			$("#std-list-tmpl").tmpl(dat).appendTo("#std-list");
	}, false);
	
}

score.get_data_from_form = function(){
	var input = {
			"msg": "",
			"error": false
		};
		
		var std_id 		= $("#std-id").val();
		var month 		= $("#month-score").val();
		var math 		= $("#std-math").val();
		var physic  	= $("#std-physic").val();
		var chemistry 	= $("#std-chemistry").val();
		var biology 	= $("#std-biology").val();
		var khmer 		= $("#std-khmer").val();
		var home 		= $("#std-home").val();
		var citizen 	= $("#std-citizen").val();
		var history 	= $("#std-history").val();
		var geography 	= $("#std-geography").val();
		var science 	= $("#std-science").val();
		var english 	= $("#std-english").val();
		var exercise 	= $("#std-exercise").val();
		var art 		= $("#std-art").val();
		var lifelesson 	= $("#std-lifelesson").val();
		var agriculture = $("#std-agriculture").val();
		
		var dividend    = $("#std-dividend").val();
		
		if(flex.isNull(month)){
			input["msg"] = "សូមបញ្ចូលប្រចាំខែ។";
			input["error"]  = true;
			$("#month-score").focus();
			return input;
		}
		
		if(flex.isNull(dividend)){
			input["msg"] = "សូមបញ្ចូលផលចែក។";
			input["error"]  = true;
			$("#month-score").focus();
			return input;
		}
		
		if(isNaN(math) || isNaN(physic) || isNaN(chemistry) || isNaN(biology) || isNaN(khmer) || isNaN(home)
				 || isNaN(citizen) || isNaN(history) || isNaN(geography) || isNaN(science) || isNaN(english)
				 || isNaN(exercise) || isNaN(art) || isNaN(lifelesson) || isNaN(agriculture) || isNaN(dividend)){
			
			input["msg"] = "សូមពិន្ទុជាលេខ។";
			input["error"]  = true;
			$("#year-score").focus();
			return input;
		}
		
		var total = parseFloat(math) + parseFloat(physic) + parseFloat(chemistry) + parseFloat(biology) + parseFloat(khmer) + parseFloat(home) 
					+ parseFloat(citizen) + parseFloat(history) + parseFloat(geography) + parseFloat(science) + parseFloat(english) 
					+ parseFloat(exercise) + parseFloat(art) + parseFloat(lifelesson) + parseFloat(agriculture);
		var average = 0;
		if(total != 0)
			average = parseFloat(total) / parseFloat(dividend);
		
		$("#std-total").val(total);
		$("#std-average").val(average);
		
		input = {
				'STD_ID'		: std_id,
				'MONTH_SCORE' 	: month,
				'MATH'  		: math,
				'PHYSIC'		: physic,
				'CHEMISTRY'		: chemistry,
				'BIOLOGY'		: biology,
				'KHMER'			: khmer,
				'HOME' 			: home,
				'CITIZEN'		: citizen,
				'HISTORY'		: history,
				'GEOGRAPHY'		: geography,
				'SCIENCE'		: science,
				'ENGLISH'		: english,
				'EXERCISE'		: exercise,
				'ART'			: art,
				'LIFELESSON'	: lifelesson,
				'AGRICULTURE'	: agriculture,
				'TOTAL'			: total,
				'DIVIDEND'		: dividend,
				'AVERAGE'		: average
		}
		
		return input;
}