
var classes = {};
var isEdit = false;

flex.extend(
{
	//TODO: Onload Js
	onload: function()
	{
		
	},
	
	//TODO: On Event Js
	events: function()
	{
		$(document).on("keypress", ".form-input", function(key){
			var keycode = (key.keyCode ? key.keyCode : key.which);
			if(keycode == '13'){
				$("#btn-submit").trigger("click").focus();	      
			}
		});
		
		$(document).on("click", "#btn-submit", function(){
			
			if(isEdit == true){
				classes.ajax_class_update();
			}
			else
			{
				var input = classes.get_class_from_form();
				
				if(input["error"])
				{
					alert(input["msg"]);
					return;
				}
				if(input != 0){
					
					comm.request.createAjaxRequest(input, save_cls, "GET", function(dat)
					{
						if(!dat.ERROR){
							classes.get_classes_list();
							$("#result").text(dat.MSG);
							$("input[type=text]").val("");
						}
						else
						{
							$("#result").text(dat.MSG);
						}
					}, false);
				}
			}
		});
		
		$(document).on("dblclick", ".data-rows", function(){
			var clsId = $(this).closest(".data-rows").find(".data-search").text();
			classes.display_class_to_form(clsId);
		});
	}
	
});

classes.ajax_class_update = function()
{
	var id = $("#id").val();
	var input = classes.get_class_from_form();
	input["ID"] = id;
	
	if(input["error"])
	{
		alert(input["msg"]);
		return;
	}

	comm.request.createAjaxRequest(input, update_cls, "GET", function(dat){
		if(!dat.ERROR){
			classes.get_classes_list();
			$("#result").text(dat.MSG);
			$("input[type=text]").val("");
		}
		else
		{
			$("#result").text(dat.MSG);
		}
	}, false);
}

classes.display_class_to_form = function(id)
{
	var input = {
		"ID": id
	};
	isEdit = true;
	
	comm.request.createAjaxRequest(input, get_cls_by_id, "GET", function(dat){
		$("#id").val(dat[0].id);
		$("#class-name").val(dat[0].class_nm);
		$("#study-year").val(dat[0].year);
	}, false);
}

classes.get_classes_list = function()
{
	var input = '';
	comm.request.createAjaxRequest(input, ajax_cls_list, "GET", function(dat)
	{
		$("#cls-list").empty();
		
		$("tfoot").hide();
		
		$.map(dat, function(val, i){
			val["no"] = i + 1;
			val["href"] = base_url + "/" + val["id"];
			return val;
		});
		
		$("#cls-list-tmpl").tmpl(dat).appendTo("#cls-list");
	}, false);
}

classes.get_class_from_form = function()
{
	var input = {
		"msg": "",
		"error": false
	};
	
	var class_nm 			= $("#class-name").val();
	var year  				= $("#study-year").val();
	
	if(flex.isNull(class_nm)){
		input["msg"] = "ឈ្មោះថ្នាក់មិនត្រឹមត្រូវ។";
		input["error"]  = true;
		$("#class-name").focus();
		return input;
	}
	if(flex.isNull(year)){
		input["msg"] = "ឆ្នាំសិក្សាមិនត្រឹមត្រូវ។";
		input["error"]  = true;
		$("#study-year").focus();
		return input;
	}
	
	input = {
			"CLASS_NM": class_nm,
			"YEAR" : year
	}
	
	return input;
	
}