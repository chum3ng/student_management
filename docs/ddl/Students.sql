DROP TABLE IF EXISTS sms_students;
create table sms_students
(
	id   					integer   	  PRIMARY KEY AUTO_INCREMENT,
	std_nm   				varchar(100)  not null,
	std_dob      			varchar(20)   not null,
	std_gender      		char(1)       not null default 1,
	std_father_name 		varchar(100)  not null,
	std_father_job  		varchar(100)  not null,
	std_father_phone    	varchar(100),
	std_mother_name    		varchar(100), 
	std_mother_job    		varchar(100),
	std_mother_phone  		varchar(100),
	std_birth_village 		varchar(100),
	std_birth_commune 		varchar(100)  not null,
	std_birth_district		varchar(100)  not null,
	std_birth_province 		varchar(100)  not null,
	std_now_village 		varchar(100),
	std_now_commune 		varchar(100)  not null,
	std_now_district 		varchar(100)  not null,
	std_now_province 		varchar(100)  not null,
	class_id				integer
);

ALTER TABLE sms_students ADD CONSTRAINT fk_classes FOREIGN KEY(class_id) REFERENCES sms_classes(id);
