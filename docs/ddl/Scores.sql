DROP TABLE IF EXISTS public.sms_scores;
create table sms_scores
(
	id   					integer   	  PRIMARY KEY AUTO_INCREMENT,
	month_score				varchar(10) not null,
	year_score				varchar(4) not null,
	std_math   				numeric(5,2),
	std_physic				numeric(5,2),
	std_chemistry  			numeric(5,2),
	std_biology				numeric(5,2),
	std_khmer   			numeric(5,2),
	std_home				numeric(5,2),
	std_citizen   			numeric(5,2),
	std_history				numeric(5,2),
	std_geography   		numeric(5,2),
	std_science				numeric(5,2),
	std_english   			numeric(5,2),
	std_exercise			numeric(5,2),
	std_art   				numeric(5,2),
	std_lifelesson			numeric(5,2),
	std_agriculture			numeric(5,2),
	dividend				integer,
	std_total   			numeric(6,2),
	std_average				numeric(5,2),
	std_id 					integer
);

ALTER TABLE sms_scores ADD CONSTRAINT fk_scores FOREIGN KEY(std_id) REFERENCES sms_students(id);