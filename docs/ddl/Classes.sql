DROP TABLE IF EXISTS public.sms_classes;
create table sms_classes
(
	id   					integer   	  PRIMARY KEY AUTO_INCREMENT,
	class_nm   				varchar(255)  not null,
	year					varchar(20)   not null
);